-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2018 at 01:12 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anlandpremium`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `email`, `phone`, `message`, `created_at`, `updated_at`, `ip`, `region`, `status`) VALUES
(20, 'A1-B1', 'duongvu.survive@gmail.com', '9792326582', 'Subcribe', '2018-09-24 03:00:05', '2018-09-24 03:00:05', '::1', ' , . Vĩ độ  | Kinh độ ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `imageproperty`
--

CREATE TABLE `imageproperty` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imageproperty`
--

INSERT INTO `imageproperty` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'mat-bang.jpg', '2018-09-19 02:02:04', '2018-09-19 02:02:04');

-- --------------------------------------------------------

--
-- Table structure for table `image_house`
--

CREATE TABLE `image_house` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_house`
--

INSERT INTO `image_house` (`id`, `name`, `location`, `status`, `description`, `created_at`, `updated_at`) VALUES
(1, 'anland-premium-nam-cuong.jpg', 'bg1', 1, 'anland-premium-nam-cuong', '2018-09-18 23:33:40', '2018-09-18 23:33:40'),
(2, 'chung-cu-anland-premium-duong-noi1.jpg', 'bg2', 1, 'chung-cu-anland-premium-duong-noi1', '2018-09-18 23:33:50', '2018-09-18 23:33:50'),
(3, 'noi-that.jpg', 'bg3', 1, 'noi-that', '2018-09-18 23:34:02', '2018-09-18 23:34:02'),
(4, 'bg04.jpg', 'bg4', 1, 'bg04', '2018-09-22 08:43:25', '2018-09-22 08:43:25'),
(5, 'thiet-ke-can-ho-anland-nam-cuong.jpg', 'house', 1, 'thiet-ke-can-ho-anland-nam-cuong', '2018-09-22 09:57:46', '2018-09-22 09:57:46'),
(6, 'thiet-ke-can-ho-anland-nam-cuong-04.jpg', 'house', 1, 'thiet-ke-can-ho-anland-nam-cuong', '2018-09-22 09:57:58', '2018-09-22 09:57:58'),
(7, 'thiet-ke-can-ho-anland-nam-cuong-05.jpg', 'house', 1, 'thiet-ke-can-ho-anland-nam-cuong', '2018-09-22 09:58:09', '2018-09-22 09:58:09'),
(8, 'thiet-ke-can-ho-anland-premium.jpg', 'house', 1, 'thiet-ke-can-ho-anland-premium', '2018-09-22 09:58:31', '2018-09-22 09:58:31'),
(9, 'banner-anland-premium-moi-nhat.png', 'banner', 1, 'banner-anland-premium-moi-nhat', '2018-09-24 01:58:57', '2018-09-24 01:58:57');

-- --------------------------------------------------------

--
-- Table structure for table `locationpro`
--

CREATE TABLE `locationpro` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `propertyId` int(11) NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageId` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locationpro`
--

INSERT INTO `locationpro` (`id`, `name`, `propertyId`, `location`, `imageId`, `created_at`, `updated_at`) VALUES
(3, 'A1-B1', 5, '337,222,430,224,437,49,362,47,357,64,338,66,338,107,318,107,320,154,320,202,333,205', 2, '2018-09-19 02:42:52', '2018-09-19 02:42:52'),
(4, 'A12-B13', 12, '462,51,564,51,568,176,561,180,554,203,459,200', 2, '2018-09-19 02:45:38', '2018-09-19 02:45:38'),
(5, 'A2-B2', 6, '335,225,337,353,355,358,359,375,432,375,433,227', 2, '2018-09-19 02:46:13', '2018-09-19 02:46:13'),
(6, 'A2-B2', 6, '460,205,464,356,535,354,535,332,559,334,562,207', 2, '2018-09-19 02:47:32', '2018-09-19 02:47:32'),
(7, 'A2-B2', 6, '360,378,433,378,433,531,338,532,340,407,360,402', 2, '2018-09-19 02:48:13', '2018-09-19 02:48:13'),
(8, 'A1-B1', 5, '335,536,335,558,320,558,321,647,337,651,337,692,359,695,359,710,435,707,432,532', 2, '2018-09-19 02:49:41', '2018-09-19 02:49:41');

-- --------------------------------------------------------

--
-- Table structure for table `metas`
--

CREATE TABLE `metas` (
  `id` int(10) UNSIGNED NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `metas`
--

INSERT INTO `metas` (`id`, `location`, `status`, `userId`, `content`, `created_at`, `updated_at`) VALUES
(1, 'home', 0, 1, '1', '2018-09-22 17:11:18', '2018-09-24 03:26:55'),
(2, 'post', 0, 2, '<meta >??', '2018-09-24 03:21:23', '2018-09-24 03:26:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_11_081249_add_phone_into_users', 1),
(4, '2018_09_11_081401_add_premission_into_users', 1),
(5, '2018_09_11_081510_add_image_into_users', 1),
(6, '2018_09_14_101512_create_posts_table', 1),
(7, '2018_09_15_103929_create_meta_table', 1),
(8, '2018_09_15_144333_create_project_table', 1),
(9, '2018_09_16_120837_create_customer_table', 1),
(10, '2018_09_16_160953_create_image_house_table', 1),
(11, '2018_09_18_123105_add_url_into_posts', 1),
(12, '2018_09_18_130309_create_property_table', 1),
(17, '2018_09_19_070940_create_image_porperty_table', 2),
(18, '2018_09_19_073037_create_location_pro_table', 2),
(19, '2018_09_19_131058_add_ip_region_into_customer', 3),
(20, '2018_09_22_171555_add_status_innto_customer', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('duongvu.survive@gmail.com', '$2y$10$olM2ZEx8qmZYKilclUdV3uo1MyhSt0QM5/pE06GjCNnXmvbVBvCsi', '2018-09-24 02:44:04');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` int(11) NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `thumb` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `description`, `userId`, `content`, `status`, `thumb`, `keywords`, `created_at`, `updated_at`, `url`) VALUES
(1, 'my title', 'x', 1, '<p>x<img src=\"/lte/public/photos/shares/apartment/noi-that.jpg\" alt=\"\" /></p>', 1, 'jQzUY_chung-cu-anland-premium-duong-noi1.jpg', '[\"x\"]', '2018-09-19 08:10:38', '2018-09-22 10:05:00', 'my-title'),
(2, 'D’. El Dorado – Vùng đất an lành giữa lòng thành phố', 'Nắm bắt được mong muốn sở hữu một căn hộ ven Hồ Tây, Tập đoàn Tân Hoàng Minh đã cho ra mắt sản phẩm D’. El Dorado…\r\nHồ Tây luôn luôn là thắng cảnh nổi tiếng của đất Hà thành. Vị trí độc đắc về phong thủy, nơi quy tụ truyền thuyết và thần thoại hàng nghìn năm,… tất cả những điều này đã khiến cho Hồ Tây trở thành nơi linh thiêng, nơi đáng sống bậc nhất đất kinh kỳ Thăng Long.', 1, '<p>Kh&ocirc;ng chỉ l&agrave; mảnh đất t&acirc;m linh v&agrave; phong thủy bậc nhất Việt Nam, hồ T&acirc;y c&ograve;n l&agrave; nơi h&ograve;a quyện cả cảnh quan v&agrave; bầu kh&ocirc;ng kh&iacute; trong l&agrave;nh mang đến cho người d&acirc;n ở đ&acirc;y chất lượng cuộc sống cao.Với hơn 500 h&eacute;c ta diện t&iacute;ch mặt nước c&ugrave;ng h&agrave;ng ng&agrave;n c&acirc;y liễu chạy dọc ven hồ, Hồ T&acirc;y được v&iacute; như l&aacute; phổi xanh của Thủ đ&ocirc; gi&uacute;p điều h&ograve;a v&agrave; thanh lọc kh&ocirc;ng kh&iacute; cho cả một v&ugrave;ng nội th&agrave;nh rộng lớn.</p>\r\n<p>Kh&ocirc;ng phải tự nhi&ecirc;n m&agrave; nhiều người mong ước sở hữu một mảnh &ldquo;đất v&agrave;ng&rdquo; tại hồ T&acirc;y nhưng c&aacute;i kh&oacute; l&agrave; đất ở hồ T&acirc;y lu&ocirc;n ở t&igrave;nh trạng đắt v&agrave; khan hiếm bởi t&acirc;m l&yacute; kh&ocirc;ng muốn b&aacute;n của những cư d&acirc;n đang sinh sống tại đ&acirc;y</p>\r\n<p>V&igrave; lẽ đ&oacute;, sở hữu mảnh đất tại nơi trung t&acirc;m của H&agrave; Nội l&agrave; mong muốn của rất nhiều người.</p>\r\n<p>Nắm bắt được mong muốn sở hữu một căn hộ ven Hồ T&acirc;y, Tập đo&agrave;n T&acirc;n Ho&agrave;ng Minh đ&atilde; cho ra mắt sản phẩm D&rsquo;. El Dorado. Kể từ khi ra mắt, sản phẩm đ&atilde; tạo ra một sức h&uacute;t tr&ecirc;n thị trường bất động sản v&agrave; thu h&uacute;t sự quan t&acirc;m của rất nhiều kh&aacute;ch h&agrave;ng v&agrave; những nh&agrave; đầu tư.</p>\r\n<p>Tọa lạc tại ng&atilde; tư đường V&otilde; Ch&iacute; C&ocirc;ng v&agrave; Nguyễn Ho&agrave;ng T&ocirc;n, D&rsquo;. El Dorado được coi l&agrave; mảnh đắc địa của khu vực T&acirc;y Hồ T&acirc;y. Với hạ tầng giao th&ocirc;ng hiện đại, di chuyển thuận tiện tới s&acirc;n bay Nội B&agrave;i v&agrave; nhiều khu vực kh&aacute;c trong th&agrave;nh phố. Đ&acirc;y l&agrave; khu vực được quy hoạch trở th&agrave;nh trung t&acirc;m th&agrave;nh phố với nhiều tiện &iacute;ch v&agrave; dịch vụ c&ocirc;ng cộng trong tương lai.</p>', 1, 'wLoXJ_anland-premium-nam-cuong.jpg', '[\"123\",\"\"]', '2018-09-21 04:34:47', '2018-09-21 04:57:36', 'd-el-dorado-vung-dat-an-lanh-giua-long-thanh-pho');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `policy` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ground` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `name`, `introduction`, `description`, `payment`, `policy`, `ground`, `content`, `location`, `created_at`, `updated_at`) VALUES
(1, 'Anland Premium', '<p><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipisicing elit, sed do eiusmod&nbsp; tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp; quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo&nbsp; consequat.&nbsp;</p>', '<p><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipisicing elit, sed do eiusmod<br />\r\n&nbsp; &nbsp; tempor incididunt ut labore et dolore magna aliqua.</p>\r\n\r\n<ul>\r\n	<li>Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</li>\r\n	<li>&nbsp; &nbsp; <em>consequat. </em></li>\r\n	<li>Duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur.</li>\r\n	<li>Excepteur sint occaecat cupidatat non&nbsp;proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>\r\n</ul>', '<p style=\"text-align:center\"><strong>NHẬN TRỌN BỘ T&Agrave;I LIỆU &amp; CH&Iacute;NH S&Aacute;CH B&Aacute;N H&Agrave;NG MỚI NHẤT</strong></p>\r\n\r\n<p><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipisicing elit, sed do eiusmod<br />\r\n&nbsp; &nbsp; tempor incididunt ut labore et dolore magna aliqua.</p>\r\n\r\n<ul>\r\n	<li>Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</li>\r\n	<li>&nbsp; &nbsp; <em>consequat. </em></li>\r\n	<li>Duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur.</li>\r\n	<li>Excepteur sint occaecat cupidatat non&nbsp;proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>\r\n</ul>', '<p>Gi&aacute; b&aacute;n căn hộ Anland Premium từ&nbsp;<strong>25,7 &ndash; 29,96 tr/m2</strong>&nbsp;(đơn gi&aacute; tr&ecirc;n diện t&iacute;ch th&ocirc;ng thủy đ&atilde; bao gồm VAT v&agrave; chưa gồm 2% ph&iacute; bảo tr&igrave;).</p>\r\n\r\n<p><strong>Đặt cọc:</strong>&nbsp;<strong>50.000.000đ</strong></p>\r\n\r\n<p><strong>Đợt 1:</strong>&nbsp;<strong>10%&nbsp;</strong>tổng gi&aacute; trị căn hộ (bao gồm&nbsp;<strong>50 triệu</strong>&nbsp;đặt cọc) &ndash; Trong v&ograve;ng&nbsp;<strong>15</strong>ng&agrave;y kể từ ng&agrave;y đặt cọc.</p>\r\n\r\n<p><strong>Đợt 2:</strong>&nbsp;<strong>10%&nbsp;</strong>tổng gi&aacute; trị căn hộ &ndash; Khi k&yacute; hợp đồng mua b&aacute;n.</p>\r\n\r\n<p><strong>Đợt 3:&nbsp;10%&nbsp;</strong>gi&aacute; trị căn hộ &ndash; Sau&nbsp;<strong>3</strong>&nbsp;th&aacute;ng kể từ ng&agrave;y k&yacute; HĐMB.</p>\r\n\r\n<p><strong>Đợt 4:</strong>&nbsp;<strong>10%&nbsp;</strong>gi&aacute; trị căn hộ &ndash; Sau&nbsp;<strong>6</strong>&nbsp;th&aacute;ng kể từ ng&agrave;y k&yacute; HĐMB.</p>\r\n\r\n<p><strong>Đợt 5:</strong>&nbsp;<strong>10%&nbsp;</strong>gi&aacute; trị căn hộ &ndash; Sau&nbsp;<strong>9</strong>&nbsp;th&aacute;ng kể từ ng&agrave;y k&yacute; HĐMB.</p>\r\n\r\n<p><strong>Đợt 6:</strong>&nbsp;<strong>10%&nbsp;</strong>gi&aacute; trị căn hộ &ndash; Sau&nbsp;<strong>12</strong>&nbsp;th&aacute;ng kể từ ng&agrave;y k&yacute; HĐMB.</p>\r\n\r\n<p><strong>Đợt 7:</strong>&nbsp;<strong>10%&nbsp;</strong>gi&aacute; trị căn hộ &ndash; Sau&nbsp;<strong>15</strong>&nbsp;th&aacute;ng kể từ ng&agrave;y k&yacute; HĐMB.</p>\r\n\r\n<p><strong>Đợt 8:</strong>&nbsp;<strong>25%</strong>&nbsp;gi&aacute; trị căn hộ +&nbsp;<strong>2%</strong>&nbsp;ph&iacute; bảo tr&igrave; &ndash; Khi nhận b&agrave;n giao nh&agrave;.</p>\r\n\r\n<p><strong>Đợt 9:&nbsp;5%&nbsp;</strong>gi&aacute; trị căn hộ &ndash; Khi nhận sổ hồng.</p>', '<div class=\"row\">\r\n<div class=\"col-md-1\">&nbsp;</div>\r\n\r\n<div class=\"col-md-3\">\r\n<div class=\"cc\">\r\n<i class=\'fa fa-hotel\'></i>\r\n<h5>Kiến tr&uacute;c</h5>\r\n\r\n<p>Anland Premium c&oacute; thiết kế tối giản m&agrave; lịch l&atilde;m, hiện đại m&agrave; ph&oacute;ng kho&aacute;ng, tối ưu c&ocirc;ng năng sử dụng m&agrave; vẫn gần gũi với thi&ecirc;n nhi&ecirc;n.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-4\">&nbsp;</div>\r\n\r\n<div class=\"col-md-3\">\r\n<div class=\"cc\">\r\n<i class=\'fa fa-pagelines\'></i>\r\n<h5>Uy t&iacute;n người đồng h&agrave;nh</h5>\r\n\r\n<p>Định hướng ph&aacute;t triển c&aacute;c sản phẩm bất động sản ở ph&acirc;n kh&uacute;c cao cấp, th&acirc;n thiện với m&ocirc;i trường, Tập đo&agrave;n Nam Cường lu&ocirc;n ch&uacute; trọng cải tiến c&ocirc;ng nghệ để kiến tạo n&ecirc;n c&aacute;c dự &aacute;n xanh, cao cấp, độc đ&aacute;o, với m&ocirc;i trường sinh th&aacute;i ho&agrave;n hảo, mang lại gi&aacute; trị gia tăng cho nhiều thế hệ kh&aacute;ch h&agrave;ng.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-1\">&nbsp;</div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-1\">&nbsp;</div>\r\n\r\n<div class=\"col-md-3\">\r\n<div class=\"cc\">\r\n<i class=\'fa fa-tree\'></i>\r\n<h5>Xanh chuẩn quốc tế</h5>\r\n\r\n<p>Được Ban quản l&yacute; dự &aacute;n EECB (UNDP) v&agrave; Bộ X&acirc;y dựng lựa chọn tr&igrave;nh diễn c&ocirc;ng tr&igrave;nh tiết kiệm năng lượng trong c&aacute;c t&ograve;a nh&agrave; thương mại v&agrave; chung cư cao tầng tại Việt Nam. Anland Premium x&acirc;y dựng theo ti&ecirc;u chuẩn chứng chỉ xanh Quốc tế EDGE của IFC Ng&acirc;n h&agrave;ng thế giới.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-4\">&nbsp;</div>\r\n\r\n<div class=\"col-md-3\">\r\n<div class=\"cc\">\r\n<i class=\'fa fa-globe-africa\'></i>\r\n<h5>Cộng đồng văn minh trong m&ocirc;i trường sống bền vững</h5>\r\n\r\n<p>Với lối sống Xanh An L&agrave;nh, Anland Premium mang đến cho cư d&acirc;n cuộc sống hiện đại, bền vững, an to&agrave;n v&agrave; th&acirc;n thiện, một m&ocirc;i trường ph&aacute;t triển to&agrave;n diện Nh&acirc;n - Tr&iacute; - Lực cho con trẻ trong cộng đồng văn minh tri thức.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-1\">&nbsp;</div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-1\">&nbsp;</div>\r\n\r\n<div class=\"col-md-3\">\r\n<div class=\"cc\">\r\n<i class=\'fa fa-heart\'></i>\r\n<h5>Gi&aacute; trị tăng theo thời gian</h5>\r\n\r\n<p>AnC&ugrave;ng với t&ecirc;n tuổi uy t&iacute;n của Chủ đầu tư Nam Cường, Anland Premium g&oacute;p phần kiến tạo v&agrave; ph&aacute;t triển gi&aacute; trị bất động sản xanh, mang lại lợi nhuận kinh tế l&acirc;u d&agrave;i v&agrave; bền vững cho gia chủ.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-4\">&nbsp;</div>\r\n\r\n<div class=\"col-md-3\">\r\n<div class=\"cc\">\r\n<i class=\'fa fa-archway\'></i>\r\n<h5>Tiện t&iacute;ch to&agrave;n diện</h5>\r\n\r\n<p>L&agrave; dự &aacute;n nằm trong &ldquo;Township&rdquo;, Anland Premium hưởng trọn vẹn tiện &iacute;ch đồng bộ của nội v&agrave; ngoại Khu đ&ocirc; thị sinh th&aacute;i Dương Nội, đ&aacute;p ứng tối đa nhu cầu sinh hoạt cho cả gia đ&igrave;nh, đặc biệt l&agrave; những gia đ&igrave;nh trẻ.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-1\">&nbsp;</div>\r\n</div>', '<h3 style=\"text-align:center\">Tiện &iacute;ch nội khu</h3>\r\n\r\n<hr />\r\n<div class=\"row\" style=\"padding:20px 5px\">\r\n<div class=\"col-md-4\">\r\n<ul>\r\n	<li>&nbsp;Vườn c&acirc;y xanh</li>\r\n	<li>&nbsp;Khu thương mại cho thu&ecirc;.</li>\r\n	<li>&nbsp;Trung t&acirc;m tập thể h&igrave;nh</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"col-md-4\">\r\n<ul>\r\n	<li>&nbsp;Bể bơi ngo&agrave;i trời</li>\r\n	<li>&nbsp;Thư viện s&aacute;ch.</li>\r\n	<li>&nbsp;Ph&ograve;ng tập yoga</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"col-md-4\">\r\n<ul>\r\n	<li>&nbsp;Hầm để xe th&ocirc;ng minh.</li>\r\n	<li>&nbsp;Nh&agrave; trẻ mẫu gi&aacute;o.</li>\r\n	<li>&nbsp;Khu vườn hoa.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/lte/public/photos/shares/photos/tien-ich-noi-khu-anland-premium-01.jpg\" /></p>\r\n\r\n<div style=\"page-break-after:always\"><span style=\"display:none\">&nbsp;</span></div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Tiện &iacute;ch ngoại khu</h3>\r\n\r\n<hr />\r\n<div class=\"row\" style=\"padding:20px 5px\">\r\n<div class=\"col-md-3\">\r\n<ul>\r\n	<li>C&ocirc;ng vi&ecirc;n thi&ecirc;n văn học + Hồ điều h&ograve;a rộng 12ha.</li>\r\n	<li>&nbsp;C&ocirc;ng Vi&ecirc;n c&acirc;y xanh rộng 2,5ha.</li>\r\n	<li>&nbsp;Trung t&acirc;m tổ chức tiệc cưới.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"col-md-3\">\r\n<ul>\r\n	<li>&nbsp;Hệ thống trường học quốc tế.</li>\r\n	<li>&nbsp;Bệnh viện quốc tế Nam Cường.</li>\r\n	<li>&nbsp;S&acirc;n đ&aacute;nh tennis, C&acirc;u l&ocirc;ng.</li>\r\n	<li>&nbsp;Bến du thuyền</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"col-md-3\">\r\n<ul>\r\n	<li>&nbsp;Khu biệt thự TM An Ph&uacute; Shop Villa</li>\r\n	<li>&nbsp;Khu biệt thự An Khang Villa</li>\r\n	<li>&nbsp;Rạp chiếu phim.</li>\r\n	<li>&nbsp;Ch&ugrave;a T&acirc;m Linh</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"col-md-3\">\r\n<ul>\r\n	<li>&nbsp;Thư viện đ&ocirc; thị</li>\r\n	<li>&nbsp;Khu vườn Cafe</li>\r\n	<li>&nbsp;Hệ thống đường đi bộ</li>\r\n	<li>&nbsp;Nh&agrave; Trẻ</li>\r\n	<li>&nbsp;Aeon Mall H&agrave; Đ&ocirc;ng.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<p><img alt=\"tien-ich-ngoai-khu-anland-premium-1\" src=\"http://localhost/lte/public/photos/shares/photos/tien-ich-ngoai-khu-anland-premium-1.jpg\" /></p>', '<p><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipisicing elit, sed do eiusmod<br />\r\n&nbsp; &nbsp; tempor incididunt ut labore et dolore magna aliqua.</p>\r\n\r\n<ul>\r\n	<li>Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</li>\r\n	<li>&nbsp; &nbsp; <em>consequat. </em></li>\r\n	<li>Duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur.</li>\r\n	<li>Excepteur sint occaecat cupidatat non&nbsp;proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>\r\n</ul>', '2018-09-18 23:39:47', '2018-09-24 02:12:02');

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE `property` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `name`, `content`, `created_at`, `updated_at`) VALUES
(5, 'A1-B1', '<p><img alt=\"\" src=\"http://localhost/lte/public/photos/shares/property/a1-b1.jpg\" /></p>', '2018-09-19 02:27:49', '2018-09-19 04:04:39'),
(6, 'A2-B2', '<p><img alt=\"\" src=\"http://localhost/lte/public/photos/shares/property/a2-b2.jpg\" style=\"height:680px; width:816px\" /></p>', '2018-09-19 02:34:57', '2018-09-19 02:34:57'),
(7, 'A5-B5', '<p><img alt=\"\" src=\"http://localhost/lte/public/photos/shares/property/a5-b5.jpg\" style=\"height:706px; width:761px\" /></p>', '2018-09-19 02:35:16', '2018-09-19 02:35:16'),
(8, 'A7', '<p><img alt=\"\" src=\"http://localhost/lte/public/photos/shares/property/a7.jpg\" style=\"height:679px; width:834px\" /></p>', '2018-09-19 02:35:32', '2018-09-19 02:35:32'),
(9, 'A8-B9', '<p><img alt=\"\" src=\"http://localhost/lte/public/photos/shares/property/a8-b9.jpg\" style=\"height:690px; width:816px\" /></p>', '2018-09-19 02:35:51', '2018-09-19 02:35:51'),
(10, 'A9-B10', '<p><img alt=\"\" src=\"http://localhost/lte/public/photos/shares/property/a9-b10.jpg\" style=\"height:677px; width:863px\" /></p>', '2018-09-19 02:36:11', '2018-09-19 02:36:11'),
(11, 'A10-B11', '<p><img alt=\"\" src=\"http://localhost/lte/public/photos/shares/property/a10-b11.jpg\" style=\"height:677px; width:836px\" /></p>', '2018-09-19 02:36:38', '2018-09-19 02:36:38'),
(12, 'A12-B13', '<p><img alt=\"\" src=\"http://localhost/lte/public/photos/shares/property/a12-b13.jpg\" style=\"height:684px; width:827px\" /></p>', '2018-09-19 02:36:56', '2018-09-19 02:36:56'),
(13, 'B8', '<p><img alt=\"\" src=\"http://localhost/lte/public/photos/shares/property/b8.jpg\" style=\"height:679px; width:876px\" /></p>', '2018-09-19 02:37:14', '2018-09-19 02:37:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `premission` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `phone`, `premission`, `image`) VALUES
(1, 'duongvu123', 'duongvu.survive@gmail.com', NULL, '$2y$10$fb/QoVN7iAYgiQMHexNqiuNHjL2djOoysJZy9dsa8OSsVDovjtov6', 'P0acI52yr9obJXAnv2hZDupdEy5Kq0OtmC2e5Wb163vlnzmLpnf68bhh9Y09', '2018-09-18 23:29:14', '2018-09-24 02:38:52', '01238859868', 'admin', 'user.jpg'),
(2, 'duongvu', 'ndiem3620@gmail.com', NULL, '$2y$10$L4/GBIM5P0r7tsNjN0RCkOtom2Ri2sYqGkFLTgC/z6ddjAAgNFMse', NULL, '2018-09-24 03:01:25', '2018-09-24 03:01:25', '9792326582', 'employee', 'user.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imageproperty`
--
ALTER TABLE `imageproperty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_house`
--
ALTER TABLE `image_house`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locationpro`
--
ALTER TABLE `locationpro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metas`
--
ALTER TABLE `metas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `imageproperty`
--
ALTER TABLE `imageproperty`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `image_house`
--
ALTER TABLE `image_house`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `locationpro`
--
ALTER TABLE `locationpro`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `metas`
--
ALTER TABLE `metas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
