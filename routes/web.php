<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get("home",function(){
	return redirect('/admin');
});
Route::group(array('prefix'=>'admin','middleware'=>'auth'),function(){
	Route::get('/',function(){
		return redirect(url('admin/dashboard'));
	});
	Route::get('/profile','admin\homeController@profile');
	Route::post('/profile','admin\homeController@update');
	Route::get('/logout',function(){
		Auth::logout();
		return redirect(url('/login'));
	});

	Route::get('/register',function(){
		if(Auth::user()->checkPremission('admin')){
			redirect(url('/register'));
		}else
			return abort(404);
	});
	Route::get('/delete-user','admin\homeController@delete_account');
	Route::get('/delete/user/{id}','admin\homeController@delete');

	Route::get('customer','admin\customerController@customer');
	Route::get('customer/delete/{id}','admin\customerController@delete');
// =============Metas============
	Route::get("/meta",'admin\metaController@list_meta');
	Route::get("/meta/add",'admin\metaController@create_meta');
	Route::post("/meta/edit/",'admin\metaController@edit');
	Route::get("/meta/delete/{id}",'admin\metaController@delete');
	Route::get("/meta/editStatus/{id}",'admin\metaController@do_status');
	
	Route::get('dashboard','admin\homeController@dashboard');
	

	// =========Posts==========
	Route::get("/article/add",'admin\articleController@view_create');
	Route::post("/article/add",'admin\articleController@create');
	Route::get("/article",'admin\articleController@list_article');
	Route::get("/article",'admin\articleController@list_article');
	Route::get("/article/edit/{id}",'admin\articleController@edit_article');
	Route::post("/article/edit/{id}",'admin\articleController@do_edit');
	Route::get("/ajax/editStatus/{id}",'admin\articleController@do_status');
	Route::get("/article/delete/{id}",'admin\articleController@delete');
	// ========Real====
	Route::get("/real-estate/",'admin\projectController@project');
	Route::get("/real-estate/edit",'admin\projectController@edit');
	Route::post("/real-estate/edit",'admin\projectController@do_edit');
	Route::get("/real-estate/upload",'admin\projectController@upload');
	Route::get("/real-estate/upload/editStatus/{id}",'admin\projectController@editStatus');
	Route::get("/real-estate/upload/delete/{id}",'admin\projectController@delete');
	Route::post("/real-estate/upload",'admin\projectController@do_upload');


	//==============Property============
	Route::get('/property/','admin\propertyController@add');
	Route::post('/property/','admin\propertyController@do_add');
	Route::get('/location/','admin\propertyController@location');
	Route::post('/location/','admin\propertyController@do_location');
	Route::get('/image-property/','admin\propertyController@image');
	Route::post('/image-property/','admin\propertyController@do_image');

	Route::get('/image-property/delete/{id}','admin\propertyController@delete_image');
	Route::get('/property/delete/{id}','admin\propertyController@delete_property');
	Route::get('/location/delete/{id}','admin\propertyController@delete_location');


	Route::post('property/edit','admin\propertyController@edit_property');
	Route::get('site-map/','admin\homeController@sitemap');
	Route::post('site-map/','admin\homeController@upload_sm');


});
Route::group(array("prefix"=>"/",'middleware'=>'web'),function()
{
	Route::get("/","home\homeController@index");
	Route::get('recordNext/{id}','home\homeController@recordNext');
	Route::get('recordPrev/{id}','home\homeController@recordPrev');

	Route::post('send-property','home\homeController@send_property');

	Route::post('do-contact','home\homeController@contact');
	Route::get('/tin-tuc','home\homeController@posts');
	Route::get('/tin-tuc/chi-tiet/{slug}','home\homeController@detail_post');
	Route::get('/lien-he','home\homeController@lh');
});