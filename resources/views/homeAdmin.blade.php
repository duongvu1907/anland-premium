<!DOCTYPE html>
<html>
  <head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'AdminLTEitle') }}</title>
      @yield('meta')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('public/core/vendor/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('public/core/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{ asset('public/core/css/fontastic.css') }}">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="{{ asset('public/core/css/grasp_mobile_progress_circle-1.0.0.min.css') }}">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="{{ asset('public/core/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('public/core/css/style.default.css')}}">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('public/core/css/custom.css') }}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
      @yield('css')
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
      @yield('jsTop');
      <script src="{{ asset('public/core/vendor/jquery/jquery.min.js') }}"></script>
  </head>
  <body>
    <!-- Side Navbar -->
    <nav class="side-navbar">
      <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <!-- User Info-->
          <div class="sidenav-header-inner text-center"><img src="{{asset('public/upload/admin/'.Auth::user()->image)}}" alt="person" class="img-fluid rounded-circle">
            <h2 class="h5">{{Auth::user()->name}}</h2><span>{{ Auth::user()->premission}}</span>
          </div>
          <!-- Small Brand information, appears on minimized sidebar-->
          <div class="sidenav-header-logo"><a href="{{ url('admin/profile') }}" class="brand-small text-center"> <strong>B</strong><strong class="text-primary">D</strong></a></div>
        </div>
        
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
          <h5 class="sidenav-heading">Main</h5>
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li><a href="{{ url('/') }}" target="__blank"> <i class="icon-home"></i>Home</a></li>
            <li><a href="{{ url('admin/dashboard') }}"> <i class="fa fa-bar-chart"></i>Dashboard</a></li>
            <li><a href="#project" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-id-card"></i>Project</a>
              <ul id="project" class="collapse list-unstyled ">
                <li><a href="{{ url('admin/real-estate/') }}">Over View</a></li>
                <li><a href="{{ url('admin/real-estate/edit') }}">Edit</a></li>
                <li><a href="{{ url('admin/real-estate/upload') }}">Upload</a></li>
              </ul>
            </li>
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Posts</a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="{{ url('admin/article/add') }}">New Posts</a></li>
                <li><a href="{{ url('admin/article') }}">List Posts</a></li>
                <li><a href="#">Over View Project</a></li>
              </ul>
            </li>
            
             <li><a href="#property" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-circle-o"></i>Property</a>
              <ul id="property" class="collapse list-unstyled ">
                <li><a href="{{ url('admin/property/') }}">Property</a></li>
                <li><a href="{{ url('admin/location/') }}">Location</a></li>
                <li><a href="{{ url('admin/image-property/') }}">Image</a></li>
              </ul>
            </li>


            <li><a href="{{ url('admin/meta') }}"> <i class="icon-interface-windows"></i>Seo</a></li>
            <li> <a href="{{ url('admin/customer') }}"> <i class="icon-mail"></i>Inbox
                </a></li>
          </ul>
        </div>
        <div class="admin-menu">
          <h5 class="sidenav-heading">Account setting</h5>
          <ul id="side-admin-menu" class="side-menu list-unstyled"> 
            <li> <a href="{{ url('admin/profile') }}"> <i class="icon-user"></i>Profile</a></li>
           <li><a href="#settingProfile" aria-expanded="false" data-toggle="collapse"><i class="fa fa-wrench"></i>Setting</a>
              <ul id="settingProfile" class="collapse list-unstyled ">
                <?php if(Auth::user()->checkPremission('admin')){ ?>
                <li><a href="{{ url('/register') }}">Add user</a></li>
                <li><a href="{{ url('admin/delete-user/') }}">Delete Account</a></li>
                <?php } ?>
                <li><a href="#">Update Profile</a></li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="system-info">
          <h5 class="sidenav-heading">Version App</h5>
          <ul id="side-system-info" class="side-menu list-unstyled">
            <li><a href="{{ url('public/laravel-filemanager?field_name=mceu_48-inp&type=Images')}}" target="__parent"><i class="fa fa-circle-o"></i>File Manager</a></li>
            <li> <a href="#"><i class="fa fa-circle-o"></i>{{ config('app.VERSION') }}
                <div class="badge badge-info">New</div></a></li>
            <li><a href="{{  url('/admin/site-map')}}"><i class="fa fa-circle-o"></i>Sitemap</a></li>
          </ul>
        </div>

      </div>
    </nav>

    <div class="page">
      <!-- navbar-->
      <header class="header">
        <nav class="navbar">
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="index.html" class="navbar-brand">
                  <div class="brand-text d-none d-md-inline-block"><strong class="text-primary">Dashboard</strong></div></a></div>
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <!-- Notifications dropdown-->
                <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-bell"></i><span class="badge badge-warning"><?php echo DB::table('customer')->where('status','=',1)->count()!=0?DB::table('customer')->where('status','=',1)->count():'' ?></span></a>
                  <ul aria-labelledby="notifications" class="dropdown-menu">
                    <?php $custom = DB::table('customer')->where('status','=',1)->orderBy('id','desc')->take(5)->get();

                     ?>
                     <?php foreach ($custom as $c): ?>
                       
                    <li><a rel="nofollow" href="{{ url('/admin/customer') }}" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-envelope"></i> Message {{ $c->name }}</div>
                          <div class="notification-time"><small>{{ $c->updated_at }}</small></div>
                        </div></a></li>
                     <?php endforeach ?>
                    
                  </ul>
                </li>
                <!-- Messages dropdown-->
                                <!-- Languages dropdown    -->
                <!-- <li class="nav-item dropdown"><a id="languages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link language dropdown-toggle"><img src="{{ asset('public/core/img/flags/16/GB.png')}}" alt="English"><span class="d-none d-sm-inline-block">English</span></a>
                  <ul aria-labelledby="languages" class="dropdown-menu">
                    <li><a rel="nofollow" href="#" class="dropdown-item"> <img src="{{ asset('public/core/img/flags/16/DE.png')}}" alt="English" class="mr-2"><span>German</span></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item"> <img src="{{ asset('public/core/img/flags/16/FR.png')}}" alt="English" class="mr-2"><span>French                                                         </span></a></li>
                  </ul>
                </li> -->
                <!-- Log out-->
                <li class="nav-item"><a href="{{ url('admin/logout') }}" class="nav-link logout"> <span class="d-none d-sm-inline-block">Logout</span><i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <!-- Counts Section -->
      <div class="main" style="padding: 40px">
        @yield('container')
      </div>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="https://bootstrapious.com" class="external">Bootstrapious</a></p>
            </div>
          </div>
        </div>
      </footer>
    </div>
   
    <!-- JavaScript files-->
    
    <script src="{{ asset('public/core/vendor/popper.js/umd/popper.min.js') }}"> </script>
    <script src="{{ asset('public/core/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/core/js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
    <script src="{{ asset('public/core/vendor/jquery.cookie/jquery.cookie.js') }}"> </script>
    <!-- <script src="{{ asset('public/core/vendor/chart.js/Chart.min.js') }}"></script> -->
    <script src="{{ asset('public/core/vendor/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('public/core/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <!-- <script src="{{ asset('public/core/js/charts-home.js') }}"></script> -->
    <!-- Main File-->
    <script src="{{ asset('public/core/js/front.js') }}"></script>
     @yield('js')
  </body>
</html>