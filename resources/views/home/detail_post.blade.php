@extends('homePage')

@section('meta')
	 <?php foreach ($meta as $row) {
	 		echo $row->content;
	 } ?>
	 <?php $key = json_decode($post->keywords)  ?>
	 <meta name="keywords" content="<?php foreach($key as $k){ echo $k.",";}  ?>">
	 <meta  name="description" content="<?php echo $post->description ?>">
@endsection

@section('css')
<style type="text/css">
.breadcrumbs{
	padding: 20px 30px; 
}
.breadcrumbs span{
	padding: 0px 5px;
}
.pagination li{
	list-style: none;
}
</style>
@endsection

@section('main')
<div class="row">
	<div class="col-xl-1"></div>
	<div class="col-xl-10">
		<div class="breadcrumbs">
			<span>
				<a href="{{url('')}}"><i class="fa fa-home"></i>&nbsp;Trang Chủ</a>
			</span>
			<span>
				&#187;
			</span>
			<span>
				<a href="{{url('tin-tuc')}}">Tin Tức</i></a>
			</span>
			<span>
				&#187;
			</span>
			<span>
				{{ $post->title }}
			</span>
		</div>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<h2 class="h2_custom text-center" style="color: #615f5f">{{ $post->title }}</h2>
		</div>
		<div class="col-md-3"></div>
	</div>
	
</div>
	<div class="col-xl-1">
		
	</div>
</div>
<div class="row" style="background: url({{  asset('public/thumbs/'.$post->thumb) }} );background-size: cover;;">
		<div class="col-md-1"></div>
		<div class="col-md-9" style="text-indent: 50px;padding: 50px 0px;color: #fff">
			<i>&#34; <?php echo $post->description ?> &#34;</i>
		</div>
		<div class="col-md-1"></div>
</div>
<div class="row" style="padding: 20px">
	<div class="col-xl-1"></div>
	<div class="col-xl-10">
		<div class="row">
			<div class="col-md-12" style="padding: 40px;text-indent: 40px">
				Bài viết : {{$post->updated_at}} By <i><a href="" class="text-uppercase">{{ $post->user->name }}</a></i>
			</div>
		</div>
		<div class="row" style="padding: 40px;"">
			<div class="col-md-12">
				<?php echo $post->content ?>
			</div>
		</div>
	</div>
	<div class="col-xl-1"></div>
</div>
@endsection

@section('js')

@endsection
