@extends('homePage')

@section('meta')
	<?php 
	foreach ($meta as $row) {
		echo $row->content;
	}
	 ?>
@endsection


@section('css')
<style>
p {
    line-height: 30px;
}
#tien-ich h3{
	text-align: center;
	text-transform: uppercase;
}
#tin-tuc {
    margin: 20px 30px;
    padding: 5px 10px;
}

.img-post {
    padding: 10px 0px;
    height: 250px;
    overflow: hidden;
}

.descirption-post{
    padding: 0px 16px;
    margin-top: 30px;
    margin-bottom: 20px;
}
.post{
	box-shadow: 0px 2px 8px #E6E6FA;
	padding: 0px;
	transition: .4s;
	margin-bottom: 20px;
}
.read-more{
	padding: 10px 5px;
}

@keyframes scale{
	0%{transform: scale(1);}
	100%{transform: scale(.97);}
}
.btn-news{
	padding: 10px 20px;
    background: #dead39;
    border-radius: 10px;
    line-height: 53px;
    color: #fff;
}
ul.pagination {
    padding: 0 50px;
    background: #ffffff;
    border-radius: 9px;
    box-shadow: 0px 2px 10px;
}

ul.pagination li {
    list-style: none;
    width: 100px;
    height: 30px;
    line-height: 30px;
    font-size: 30px;
}
ul.pagination li:hover{
    background:lavender;
    cursor:pointer;
}
.title-post {
    padding: 0 20px;
    margin-top: 10px;
    text-transform: uppercase;
}
h2{
	font-size:40px;
	font-family:"Lobster";
}
.project-view h4{
	color:#fff;
	font-weight: bold;
}
.detail h4{
	font-weight: lighter;
}
.stats-text{
	display: none;
}
.media{
	padding-top: 19px ;
}
@media (max-width: 1108px){
	.project-view .title{
		display: none;
	}
	.stats-text{
		display: block;
	}
	.media{
		padding: 40px 0px;
	}
}
@media (max-width: 768px){
	.project-view .title{
		display: none;
	}
	.stats-text{
		display: block;
	}
}
#dac-diem i{
	    float: left;
    font-size: 48px;
    padding: 9px;
    height: 100%;
    color: #0b5b1d;
    margin: 30px 18px;
}
.cc {
    margin: 45px 30px;
}

.cc h5 {
    font-weight: bold;
    font-size: 18px;
    color: #0b5b1d;
}

.cc p {
    font-size: 14px;
    line-height: 20px;
}
</style>
@endsection


@section('main')

<section data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="vc_section vc_custom_1536339247301">
	<div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="vc_row wpb_row row vc_row-no-padding">
		<div class="vc_column_container col-md-12 vc_custom_1536339315148">
			<div class="wpb_wrapper vc_column-inner">
				<div class="wpb_single_image wpb_content_element vc_align_center">
					<div class="wpb_wrapper">
						<div id="img-zoom" class="vc_single_image-wrapper   vc_box_border_grey">
							<img  src="{{ asset('public/photos/shares/apartment/'.$bg1->name )}}" alt="tong-quan-du-an-anland-nam-cuong-banner" title="tong-quan-du-an-anland-premium-banner"   />
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="vc_row-full-width vc_clearfix">
	</div>
	<div class="vc_row wpb_row row vc_custom_1536339135932 m-t-none m-b-none p-t-none p-b-none section-no-borders vc_row-has-fill">
		<div class="vc_column_container col-md-12 d-lg-block d-none">
			<div class="wpb_wrapper vc_column-inner">
				<div class="wpb_raw_code wpb_content_element wpb_raw_html" >
					<div class="wpb_wrapper">
						<div style="width: 30%; background-color: rgba(0, 0, 0, 0.7); padding: 40px 20px 0; color: #fff; float: left; margin-top: -650px; margin-left: 100px;">
							<p style="text-align: center; font-size: 25px; line-height: 1.3em;">
								<strong>ĐĂNG KÍ NHẬN THÔNG TIN DỰ ÁN</strong>
							</p>
							<div style="padding: 6px 0 6px 0;">
								<div role="form"  i lang="en-US" dir="ltr">
									<form action="{{ url('/do-contact') }}" method="post" >
										@csrf
										<div class="form-group" style="margin-bottom: 30px">
											<div class="custom-input-box">
												<i class="Simple-Line-Icons-user">
												</i>
												<span class="wpcf7-form-control-wrap your-name">
													<input type="text" name="name" value="" size="40" class="form-control" required="" placeholder="Họ tên*" />
												</span>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 30px">
											<div class="custom-input-box">
												<i class="Simple-Line-Icons-phone">
												</i>
												<span class="wpcf7-form-control-wrap tel-261">
													<input type="text"  name="phone"  size="40" class=" form-control" required="" placeholder="Số điện thoại*" min="9" max="12" />
												</span>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 30px">
											<div class="custom-input-box">
												<i class="Simple-Line-Icons-envelope">
												</i>
												<span class="wpcf7-form-control-wrap your-email">
													<input type="email" name="email" value="" size="40" class="form-control" required="" placeholder="Email*" />
												</span>
											</div>
										</div>
										<div class="submit-btn">
											<center>
												<input type="submit" value="ĐĂNG KÝ NGAY" class="wpcf7-form-control wpcf7-submit" />
											</center>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">
<div class="vc_row wpb_row row">
	<div class="vc_column_container col-md-12">
		<div class="wpb_wrapper vc_column-inner">
			<h2 class="vc_custom_heading h2_custom text-center ">
				{{ $project->name }}
			</h2>
			<div class="wpb_text_column wpb_content_element row" style="text-indent: 40px;padding: 20px;" >
					<?php echo $project->introduction ?>
			</div>
			</div>
		</div>
	</div>
	<div class="payment">
		<div class="vc_row wpb_row row vc_custom_1536370162235 vc_row-has-fill">
			<div class="col-md-12" style="border: dashed red;padding-bottom: 20px">
				<div class="wpb_text_column wpb_content_element " >
					<div class="wpb_wrapper" style="text-indent: 40px;padding: 20px;">

					<?php echo $project->payment ?>

					</div>
				</div>
<div class="wpb_wrapper vc_column-inner" style="background: #0a803b;padding: 10px">
	<div class="wpb_text_column wpb_content_element  vc_custom_1536342082746" >
		<div class="wpb_wrapper">
			<h3 style="text-align: center; line-height: 1.3em;color:#fff">
				<strong>NHẬN TRỌN BỘ TÀI LIỆU &amp; CHÍNH SÁCH BÁN HÀNG MỚI NHẤT</strong>
			</h3>
			<p style="text-align: center; line-height: 0.3em;">
				<span style="font-size: 22px;">
					<strong>
						<span style="color: #ff0000;">HOTLINE: 097 732 9191</span>
					</strong>
				</span>
			</p>

		</div>
	</div>
	<div role="form" class="wpcf7" style="padding-bottom: 50px;" >
		<div class="screen-reader-response">
		</div>
		<form action="{{ url('/do-contact') }}" method="post" >
			@csrf
			<div class="row rsvp-form">
				<div class="col-10 col-md-8 col-lg-3 offset-1 offset-md-2 offset-lg-0">
					<div class="form-group form-control-custom">
						<span class="wpcf7-form-control-wrap your-name">
							<input type="text" name="name" value="" size="40" class="form-control" id="contact-name" aria-required="true" aria-invalid="false" placeholder="Tên của bạn *" required="" />
						</span>
					</div>
				</div>
				<div class="col-10 col-md-8 col-lg-3 offset-1 offset-md-2 offset-lg-0">
					<div class="form-group form-control-custom">
						<span class="wpcf7-form-control-wrap your-guests">
							<input type="email" name="email" value="" size="40" class=" form-control" id="contact-guests" aria-required="true" aria-invalid="false" placeholder="Email của bạn *" required="" />
						</span>
					</div>
				</div>
				<div class="col-10 col-md-8 col-lg-3 offset-1 offset-md-2 offset-lg-0">
					<div class="form-group form-control-custom">
						<span class="wpcf7-form-control-wrap tel-19">
							<input type="number" name="phone" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="Số điện thoại *" required="" />
						</span>
					</div>
				</div>
				<div class="col-10 col-md-8 col-lg-3 offset-1 offset-md-2 offset-lg-0">
					<div class="form-group">
						<input type="submit" style="background:#ad760f" value="ĐĂNG KÝ NGAY" class="btn btn-lg custom-border-radius btn-block text-uppercase font-size-sm text-light btn-submit" />
					</div>
				</div>
			</div>
			<div class="wpcf7-response-output wpcf7-display-none">
			</div>
		</form>
	</div>
</div>
</div>
</div>		
</div>

<div  class="vc_row wpb_row row" style="margin-top: 30px;">
	<div class="vc_column_container col-md-12">
		<div class="wpb_wrapper vc_column-inner">
			<h2 style="text-align: center;" class="vc_custom_heading h2_custom">Tổng Quan {{ $project->name }}</h2>
				<div class="wpb_text_column wpb_content_element " >
				<div class="wpb_wrapper">
					<?php echo $project->description ?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>


<div  class="vc_row wpb_row vc_custom_1536344674415 section section-parallax m-t-none m-b-none section-no-borders vc_row-has-fill">
	<div class="porto-wrap-container" style="background: url({{ asset('public/photos/shares/apartment/'.$bg2->name) }});padding: 30px 40px;background-position: center;">
		<div class="row">
			<div class="vc_column_container col-md-6">
				<div class="wpb_wrapper vc_column-inner">
				</div>
			</div>
			<div class="vc_column_container col-md-6 " style="background: rgba(0,0,0,.7);padding: 10px;border-radius: 10px">
				<div class="wpb_wrapper vc_column-inner">
					<div class="vc_row wpb_row vc_inner row">
						<div class="vc_column_container col-md-12">
							<div class="wpb_wrapper vc_column-inner">
								<div class="vc_empty_space"   style="height: 32px" >
									<span class="vc_empty_space_inner">
									</span>
								</div>
									<div class="project-view row" >
										<div class="col-md-2"></div>
										<div class="col-md-4 title">
											<h4>Tên thương mại </h4>
											<h4 >Chủ đầu tư </h4>
											<h4 >Vị trí</h4>
										</div>
										<div class="col-md-4 detail">
											<h4 style="color: #0a803b;font-weight: bold;text-transform: uppercase;">{{$project->name}}</h4>
											<h4>Tập đoàn Nam Cường </h4>
											<h4>Khu đô thị Dương Nội,Hà Đông, Hà Nội</h4>
										</div>
										<div class="col-md-2"></div>
									</div>
									<div class="vc_empty_space"   style="height: 32px" >
											<span class="vc_empty_space_inner">
											</span>
									</div>
									<div class="row project-view ">
										<div class="col-md-2"></div>
										<div class="col-md-4 title">
											<h4 style="font-weight: normal;" >Diện tích lô đất </h4>
											<h4 style="font-weight: normal;">Mật độ xây dựng</h4>
											<h4 style="font-weight: normal;">Quy mô</h4>
											<h4 style="font-weight: normal;">Bãi đỗ xe</h4>
											<h4 style="font-weight: normal;">Khối đế</h4>
											<h4 style="font-weight: normal;" >Loại hình</h4>
										</div>
										
										<div class="col-md-5 detail">
											<div  class="stats-block stats-top  m-b-none ">
											<div class="stats-desc text-left" >
												<div id="counter_19667435125b983af6e588c" data-id="counter_19667435125b983af6e588c" class="stats-number" style=" color:#ffffff; font-size:20px;font-weight: lighter;color:#ffffff;" data-speed="1" data-counter-value="8570" data-separator="none" data-decimal=".">0</div>
												<div class="counter_suffix mycust" style=" font-size:20px;font-weight: lighter;color:#ffffff"> m2</div>
												<div class="stats-text" style=" color:#ffffff; font-size:20px;">Diện tích xây dựng</div>
											</div>
											</div>

											<div  class="stats-block stats-top  m-b-none media">
												<div class="stats-desc text-left">
													<div id="counter_16839372135b983af6e6713" data-id="counter_16839372135b983af6e6713" class="stats-number" style=" color:#ffffff; font-size:20px;color:#ffffff;font-weight: lighter;" data-speed="4" data-counter-value="30" data-separator="none" data-decimal=".">0</div>
													<div class="counter_suffix mycust" style=" font-size:20px;color:#ffffff;font-weight: lighter;"> %</div>
													<div class="stats-text" style=" color:#ffffff; font-size:20px;">Mật độ xây dựng</div>
												</div>
											</div>

											<div  class="stats-block stats-top  m-b-none media">
												<div class="stats-desc text-left">
													<div id="counter_14030614535b983af6e839c" data-id="counter_14030614535b983af6e839c" class="stats-number" style=" color:#ffffff; font-size:20px;color:#ffffff;font-weight: lighter;" data-speed="4" data-counter-value="25" data-separator="none" data-decimal=".">0</div>
													<div class="counter_suffix mycust" style=" font-size:20px;color:#ffffff;font-weight: lighter;"> tầng nổi </div>
													<div class="stats-text" style=" color:#ffffff; font-size:20px;">Quy mô</div>
												</div>
											</div>

											<div  class="stats-block stats-top  m-b-none media">
												<div class="stats-desc text-left">
													<div id="counter_15395486455b983af6e9a51" data-id="counter_15395486455b983af6e9a51" class="stats-number" style=" color:#ffffff; font-size:20px;color:#ffffff;font-weight: lighter;" data-speed="4" data-counter-value="25" data-separator="none" data-decimal=".">0</div>
													<div class="counter_suffix mycust" style=" font-size:20px;color:#ffffff;font-weight: lighter;"> tầng hầm</div>
													<div class="stats-text" style=" color:#ffffff; font-size:20px;">Bãi đỗ xe</div>
												</div>
											</div>

											<div  class="stats-block stats-top  m-b-none media">
												<div class="stats-desc text-left">
													<div id="counter_1178483635b983af6eb5a3" data-id="counter_1178483635b983af6eb5a3" class="stats-number" style=" color:#ffffff; font-size:20px;color:#ffffff;font-weight: lighter;" data-speed="4" data-counter-value="25" data-separator="none" data-decimal=".">0</div>
													<div class="counter_suffix mycust" style=" font-size:20px;color:#ffffff;font-weight: lighter;"> tầng thương mại dịch vụ</div>
													<div class="stats-text" style=" color:#ffffff; font-size:20px;">Khối đế</div>
												</div>
											</div>
											<div  class="stats-block stats-top  m-b-none media">
												<div class="stats-desc text-left">
													<div id="counter_2160539665b983af6ea7b9" data-id="counter_2160539665b983af6ea7b9" class="stats-number" style=" color:#ffffff; font-size:20px;color:#ffffff;font-weight: lighter;" data-speed="2" data-counter-value="575" data-separator="none" data-decimal=".">0</div>
													<div class="counter_suffix mycust" style=" font-size:20px;color:#ffffff;font-weight: lighter;"> căn hộ & 18 shophouse</div>
													<div class="stats-text" style=" color:#ffffff; font-size:20px;">Loại hình</div>
												</div>
											</div>										
	

										</div>
										<div class="col-md-1"></div>
									</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class='container'>
<div id="vi-tri" class="vc_row wpb_row row">
	<div class="vc_column_container col-md-12">
		<div class="wpb_wrapper vc_column-inner">
			<h2 style="text-align: center" class="vc_custom_heading h2_custom ">Vị Trí Chung Cư {{ $project->name }}</h2>
			<div class="vc_row wpb_row vc_inner row">
				<div class="vc_column_container col-md-6">
					<?php echo $project->location ?>
				</div>
				<div class="vc_column_container col-md-6">
					<div class="wpb_wrapper vc_column-inner">
						<div class="wpb_single_image wpb_content_element vc_align_center">
							<div class="wpb_wrapper">
								<div class="vc_single_image-wrapper vc_box_rounded  vc_box_border_grey" id="map_canvas">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.4062878725144!2d105.75930671483128!3d20.976344886026798!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345322334e5c8d%3A0x588c679fe24efb48!2sChung+c%C6%B0+AnLand+Complex!5e0!3m2!1svi!2s!4v1537181045535" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
</div>
</div>
<div class="container">
<div id="thiet-ke">
	<div class="vc_column_container">
		<div class="wpb_wrapper vc_column-inner">
			<h2 style="text-align: center" class="vc_custom_heading h2_custom ">Thiết kế căn hộ {{ $project->name }}</h2>
		</div>
	</div>
	<div class="row">
			<div class="col-md-1"></div>
			<div class="wpb_text_column wpb_content_element col-md-10" >
				<div class="wpb_wrapper">
					<p style="text-indent: 30px">Các căn hộ tại chung cư {{ $project->name }} được thiết kế với sự thân thiện với thiên nhiên đồng thời nội thất bàn giao gần như hoàn thiện. Căn hộ  được thiết kế kế tối ưu không gian, với ánh sáng được bảo phủ một lượng vừa đủ cho từng phòng ngủ tạo cho các phòng của căn hộ luôn được lưu thông không khí và sự hài hòa giữa ánh sáng và gió.</p>

				</div>
			</div>
			<div class="col-md-1"></div>
	</div>
	<div class="vc_tta-container" data-vc-action="collapse">
	<div class="porto-wrap-container row">
			<div class="col-md-12">
				<img src="{{ asset('public/photos/shares/apartment/'.$house1->name) }}" alt="{{ $house1->description }}" width="100%">
			</div>
	</div>
	<div class="row">
		<?php foreach ($houseN as $n): ?>
			<div class="col-md-4" style="padding: 10px">
			<img src="{{ asset('public/photos/shares/apartment/'.$n->name) }}" alt="{{ $n->description }}">
		</div>
		<?php endforeach ?>
	</div>
	</div>
</div>
</div>
<div id="mat-bang" class="vc_row wpb_row row">
	<div class="vc_column_container col-md-12">
		<div class="wpb_wrapper vc_column-inner">
			<h2 style="text-align: center" class="vc_custom_heading h2_custom ">Mặt bằng chung cư {{ $project->name }}</h2>
			<div class="vc_row wpb_row vc_inner row">
			<div class="map-wrapper">
			<?php foreach ($imageProperty as $img): ?>
						<img src="{{ asset('public/photos/shares/property/'.$img->name) }}" alt="MẶT-BẰNG-TỔNG-THỂ-FINAL-ICID" usemap="<?php echo '#map-'.$img->id ?>"/>
							<map name="<?php echo "map-".$img->id ?>">
								<?php foreach ($location as $l): ?>
									<?php if($l->image->id==$img->id){ ?>
								<area data-href="{{ $l->id }}" data-settings="{'fillColor': '#489A46', 'fillOpacity': '.5'}" coords="{{ $l->location }}" shape="poly" onclick="property(this)" />
									<?php } ?>
								<?php endforeach ?>
							</map>
			<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="myModal">
			<div class="modal-dialog modal-dialog-centered modal-lg">
				<div class="modal-content ">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title Lobster">Chi tiết các căn hộ {{ $project->name }}</h4>
					</div>

					<!-- Modal body -->
					<div class="modal-body" id="modal-content">
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4 text-center name-property">
								
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10 content-property">
								
							</div>
							<div class="col-md-1"></div>
						</div>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
			
				</div>
			</div>
		</div>
		<script type="text/javascript">
				 $(document).ready(function(){$('img[usemap]').imageMapWrapper({useAreaSettings:true});$('.flat-list .flat-item a').hover(function(){var hover_area=$(this).attr('fid');console.log(hover_area);$('.floor-map').find("*[aid='"+hover_area+"']").mouseover();},function(){$('.floor-map area').mouseleave();});});</script>
</div>
<div class="container">
	<section id="tien-ich">
		<div  class="vc_row wpb_row row" style="margin-top: 30px;">
	<div class="vc_column_container col-md-12">
		<div class="wpb_wrapper vc_column-inner">
			<h2 style="text-align: center;" class="vc_custom_heading h2_custom">Tiện ích {{ $project->name }}</h2>
				<div class="wpb_text_column wpb_content_element " >
				<div class="wpb_wrapper">
					<?php echo $project->content ?>
				</div>
			</div>
		</div>
	</div>
	</div>
	</section>
</div>
	<section id="dac-diem">
		<div  class="vc_row wpb_row row" style="margin-top: 30px;background: url({{ asset('public/photos/shares/apartment/'.$bg4->name) }});background-position: center center;background-repeat: no-repeat;background-size: cover;">
	<div class="vc_column_container col-md-12">
		<div class="wpb_wrapper vc_column-inner">
			<h2 style="text-align: center;margin-top: 25px; " class="vc_custom_heading h2_custom">Điểm nổi bật của {{ $project->name }}</h2>
				<div class="wpb_text_column wpb_content_element " >
				<div class="wpb_wrapper">
					<?php echo $project->ground ?>
				</div>
			</div>
		</div>
	</div>
</div>
	</section>
<div class="container">
<div id="tien-do" class="vc_row wpb_row row">
	<div class="vc_column_container col-md-12">
		<div class="wpb_wrapper vc_column-inner">
		<h2 style="text-align: center;margin-top: 30px" class="vc_custom_heading h2_custom">Tiến độ thanh toán dự án {{ $project->name }}</h2>
		</div>
		<div class="vc_row wpb_row vc_inner row">
<div class="vc_column_container col-md-6">
	<div class="wpb_wrapper vc_column-inner">
		<?php echo $project->policy ?>
	</div>

</div>


<div class="vc_column_container col-md-6">
	<div class="wpb_wrapper vc_column-inner">
		<style type="text/css">.vc_sep_line298614880:after {border-color:#dd3333 !important;}</style>
		<div class="vc_separator vc_text_separator vc_separator_align_center vc_sep_width_100 vc_sep_dashed vc_sep_pos_align_center">
			<span class="vc_sep_holder vc_sep_holder_l">
				<span class="vc_sep_line dashed vc_sep_line298614880">
				</span>
			</span>
			<h3>Đăng ký miễn phí</h3>	<span class="vc_sep_holder vc_sep_holder_r">
				<span class="vc_sep_line dashed vc_sep_line298614880">
				</span>
			</span>
		</div>
		<div class="wpb_text_column wpb_content_element " >
			<div class="wpb_wrapper">
				<p style="text-align: center;">Đăng ký ngay để nhận thông tin sớm nhất. Hotine: <strong>
					<span style="color: #ff0000;">0987 09 9191</span>
				</strong>
			</p>

		</div>
	</div>
	<div role="form" class="wpcf7" id="wpcf7-f128-p776-o5" lang="en-US" dir="ltr">
		<div class="screen-reader-response">
		</div>
		<form action="{{ url('/do-contact')}}" method="post" class="wpcf7-form" novalidate="novalidate">
			@csrf
			<div class="form-group">
				<div class="custom-input-box">
					<i class="Simple-Line-Icons-user">
					</i>
					<span class="wpcf7-form-control-wrap your-name">
						<input type="text" name="name" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" required="" placeholder="Họ tên*" />
					</span>
				</div>
			</div>
			<div class="form-group">
				<div class="custom-input-box">
					<i class="Simple-Line-Icons-phone">
					</i>
					<span class="wpcf7-form-control-wrap tel-261">
						<input type="number" name="phone" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="Số điện thoại*" required="" />
					</span>
				</div>
			</div>
			<div class="form-group">
				<div class="custom-input-box">
					<i class="Simple-Line-Icons-envelope">
					</i>
					<span class="wpcf7-form-control-wrap your-email">
						<input type="email" name="email" value="" size="40" class=" form-control" aria-required="true" aria-invalid="false" placeholder="Email*" required="" />
					</span>
				</div>
			</div>
			<div class="submit-btn">
				<center>
					<input type="submit" value="ĐĂNG KÝ NGAY" class="wpcf7-form-control wpcf7-submit" />
				</center>
			</div>
			<div class="wpcf7-response-output wpcf7-display-none">
			</div>
		</form>
	</div>
	<div class="wpb_text_column wpb_content_element  vc_custom_1536680254844" >
		<div class="wpb_wrapper">
			<h3 style="text-align: center;">
				<i class="fa fa-mobile" aria-hidden="true">
				</i> <span style="color: #ff0000;">
					<a style="color: #ff0000;" href="tel:0972644365" rel="noindex">
						<strong>0987 09 9191</strong>
					</a>
				</span>
			</h3>
			<p style="text-align: center;">&#x2b50; Gọi ngay để có <b>căn hot – giá tốt &#x2b50;</b>
			</p>
			<p style="text-align: center;">
				<b>&#x2b50; Báo giá chính xác</b> từ chủ đầu tư &#x2b50;</p>
				<p style="text-align: center;">
					<b>&#x2b50; Tư vấn chọn giải pháp tốt nhất &#x2b50;</b>
				</p>

			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
<div class="container" style="margin-top: 40px">
	<h2 style="text-align: center" class="vc_custom_heading h2_custom ">Tin Tức {{ $project->name }}</h2>
	<div class="row" id="tin-tuc">
		<?php $i=0; ?>
		<?php foreach ($post as $p): ?>
			<?php $i=$p->id ?>
		<div class="col-md-4">
			<div class="post">
				<div class="img-post" style="background: url({{ asset('public/thumbs/'.$p->thumb) }});background-size: cover;">
			</div>
			<div class="title-post">
				<?php echo $p->title ?>
			</div>
			<div class="descirption-post">
				<?php echo $p->description ?>
			</div>
			<a href="{{ url('/tin-tuc/chi-tiet/'.$p->url.'-'.$p->id) }}" class="read-more">Xem thêm ---></a>
			</div>
		</div>
		
		<?php endforeach ?>
		
	</div>
	<input type="number" hidden="" value="<?php echo $i ?>" id='recordId'>
	<div class="row" style="margin:50px">
		<div class="col-md-4 text-center">
			<ul class="pagination">
				<li class="prev" onclick="Prev()">Pre</li>
				<li class="next" onclick="Next()">Next</li>
			</ul>
		</div>
		<div class="col-md-4">
			<div class="loader"></div>
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="row" style="margin:50px">
		<div class="col-md-4"></div>
		<div class="col-md-4 text-center">
			<a href="{{ url('/tin-tuc') }}" class="btn-news">Xem tin tức mới nhất </a>
		</div>
		<div class="col-md-4"></div>
	</div>
</div>
@endsection

@section('js')
<script>
	function property(btn)
	{
		var imageId = $(btn).parents('map').attr('name');
		var locationId = $(btn).attr('data-href');
		var url = "<?php echo url('/send-property/') ?>"
		var data = {
			"id":locationId,
			"_token":"<?php echo csrf_token() ?>"
		}
		xmlhttp = new XMLHttpRequest;
		xmlhttp.onreadystatechange = function(){
			if(this.readyState==4 && this.status==200){
				var p = JSON.parse(this.responseText)[0];
				$(".name-property").html("<h2>"+p.name+"</h2>");
				$(".content-property").html(p.content);
				$("#myModal").modal('show');
			}
		}
		xmlhttp.open("post",url,true);
		xmlhttp.setRequestHeader('Content-Type', 'application/json');
		xmlhttp.send(JSON.stringify(data));

	}
	var hostname = "<?php echo url('') ?>";
	function Next()
	{
		var oldId = $("#recordId").val();
		var url = "<?php echo url('/recordNext/') ?>";
		$('.loader').html("<img src='https://demos.laraget.com/images/loading2.gif' width='50px'/>");
		$.ajax({
			url: url+"/"+oldId,
			type: 'get',
			dataType: 'html',
			data: {"oldId":oldId},
			success:function(data){
				if(data!=null){
					var posts = JSON.parse(data);
					var c=0;
					$("#tin-tuc").html("");
					posts.forEach(function(element){
						img = "<?php echo asset('public/thumbs/') ?>"+"/"+element.thumb;
						$("#tin-tuc").append("<div class='col-md-4'><div class='post'><div class='img-post' style='background: url("+img+");background-size: cover;'></div><div class='title-post'>"+element.title+"</div><div class='descirption-post'>"+element.description+"</div><a href='"+hostname+"/tin-tuc/chi-tiet/"+element.url+"-"+element.id+"' class='read-more'>Xem thêm ---></a></div></div>");
						$("#recordId").attr('value', element.id);
						
				})
					$('.loader').html('');

			}
		}
		})
		
	}
	function Prev()
	{
		var oldId = $("#recordId").val();
		var url = "<?php echo url('/recordPrev/') ?>";
		$('.loader').html("<img src='https://demos.laraget.com/images/loading2.gif' width='50px'/>");
		$.ajax({
			url: url+"/"+oldId,
			type: 'get',
			dataType: 'html',
			data: {"oldId":oldId},
			success:function(data){
				if(data!=""){
					var posts = JSON.parse(data);
					$("#tin-tuc").html("");
					posts.forEach(function(element){
						img = "<?php echo asset('public/thumbs/') ?>"+"/"+element.thumb;
						$("#tin-tuc").append("<div class='col-md-4'><div class='post'><div class='img-post' style='background: url("+img+");background-size: cover;'></div><div class='title-post'>"+element.title+"</div><div class='descirption-post'>"+element.description+"</div><a href='"+hostname+"/tin-tuc/chi-tiet/"+element.url+"-"+element.id+"' class='read-more'>Xem thêm ---></a></div></div>");
						$("#recordId").attr('value', element.id);
				});
					$('.loader').html('');

			}
		}
		})
	}
</script>
<script src="{{ asset('public/homePage/plugins/imageMap/imageMapWrapper.min.js') }}"></script>
@endsection






