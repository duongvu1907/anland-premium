@extends('homePage')

@section('meta')

@endsection

@section('css')
<style type="text/css">
	.breadcrumbs{
		padding: 20px 30px; 
	}
	.breadcrumbs span{
		padding: 0px 5px;
	}
	.pagination li{
		list-style: none;
	}
</style>
@endsection

@section('main')
<div class="row">
	<div class="col-xl-1"></div>
	<div class="col-xl-10">
	<div class="breadcrumbs">
		<span>
			<a href=""><i class="fa fa-home"></i>&nbsp;Trang Chủ</a>
		</span>
		<span>
			&#187;
		</span>
		<span>
			<a href="">Tin Tức</i></a>
		</span>
	</div>

<div class="card">
	<div class="card-header" style="background: ">
		<h2 class="h2_custom text-center" >Danh sách tin tức mới nhất</h2>
	</div>
	<div class="card-body">
		<?php foreach ($posts as $post): ?>
			<div class="row" style="margin: 35px 0px;">
				<div class="col-md-1"></div>
			<div class="col-md-4">
				<img src="{{ asset('public/thumbs/'.$post->thumb) }}" alt="{{ $createAlt->alt($post->title) }}" style="width: 100%;height: 100%">
			</div>
			<div class="col-md-6">
				<div class="title-post">
					<h4><a>{{ $post->title }}</a></h4>
				</div>
				<hr style="width: 200px;height: 5px;background:#0a803b ">
				<div class="description-post">
					<?php echo $post->description ?>
				</div>
				<a href="{{ url('/tin-tuc/chi-tiet/'.$post->url.'-'.$post->id) }}" target="__blank" class= url('/tin')"read-post">Xem chi tiết &#187;</a>
				<hr>
				<i><?php echo $post->updated_at ?></i>
			</div>
			<div class="col-md-1"></div>
		</div>
		<?php endforeach ?>
		<div class="pagination">
			{{ $posts->render()}}
		</div>
	</div>
</div>
</div>
<div class="col-xl-1"></div>
</div>
@endsection

@section('js')

@endsection
