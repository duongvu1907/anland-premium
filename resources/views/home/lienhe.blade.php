@extends('homePage')

@section('main')
<div class="page-content" style="margin :40px 20px">
	<div class="vc_row wpb_row"><div class="porto-wrap-container container"><div class="row"><div class="vc_column_container col-md-7"><div class="wpb_wrapper vc_column-inner"><h2 style="font-size: 36px" class="vc_custom_heading m-b-md align-left">Thông tin liên hệ</h2><div class="porto-separator  "><div class="divider divider-small  align_left solid "><hr  style="background-color:#288e56;height:3px;"></div></div>
	<div class="wpb_text_column wpb_content_element  lead m-b-lg" >
		<div class="wpb_wrapper">
			<p>Quý khách vui lòng điền thông tin chính xác bên dưới. Chúng tôi sẽ phản hồi lại quý khách trong thời gian sớm nhất!</p>

		</div>
	</div>
	<div role="form" class="wpcf7" id="wpcf7-f58-p769-o2" lang="en-US" dir="ltr">
		<div class="screen-reader-response"></div>
		<form action="{{ url('/do-contact') }}" method="post" class="wpcf7-form" novalidate="novalidate">
			@csrf
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<span class="wpcf7-form-control-wrap your-name"><input type="text" name="name" value="" size="40" class=" form-control input-lg" id="contact-name" aria-required="true" aria-invalid="false" placeholder="Họ và tên..." required="" /></span>
					</div></div>
					<div class="col-md-12">
						<div class="form-group">
							<span class="wpcf7-form-control-wrap your-email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control input-lg" id="contact-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ email" required="" /></span>
						</div></div>
						<div class="col-md-12">
							<div class="form-group">
								<span class="wpcf7-form-control-wrap your-subject"><input type="number" name="phone" value="" size="40" class=" form-control input-lg" id="contact-subject" aria-required="true" aria-invalid="false" placeholder="Số điện thoại" /></span>
							</div></div>
							<div class="col-md-12">
								<div class="form-group">
									<span class="wpcf7-form-control-wrap your-message"><textarea name="message" cols="40" rows="3" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control input-lg" id="contact-message" aria-required="true" aria-invalid="false" placeholder="Nội dung"></textarea></span>
								</div></div>
								<div class="col-md-12">
									<div class="form-group">
										<input type="submit" value="Gửi ngay" class="wpcf7-form-control wpcf7-submit btn btn-primary btn-lg" />
									</div></div>
								</div>
								<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div></div><div class="vc_column_container col-md-1"><div class="wpb_wrapper vc_column-inner"></div></div><div class="vc_column_container col-md-4"><div class="wpb_wrapper vc_column-inner"><h4  class="vc_custom_heading align-left">Phòng kinh doanh dự án</h4>
									<div class="wpb_text_column wpb_content_element " >
										<div class="wpb_wrapper">
											<p style="text-align: justify;">Xin chân thành cám ơn Quý khách đã quan tâm đến dự án. Để biết thêm thông tin chi tiết, Quý khách vui lòng liên hệ trực tiếp thông qua số điện thoại Hotline: <a href="tel:0972644365"><strong id="txtHotline">0972.644.365</strong></a>, hoặc điền thông tin vào bên cạnh. Chúng tôi sẽ hồi âm trong thời gian sớm nhất!</p>

										</div>
									</div>
									<h4  class="vc_custom_heading align-left">Địa chỉ liên hệ</h4><div class="porto-separator   m-b-lg"><div class="divider divider-small  align_left solid "><hr  style="background-color:#288e56;height:3px;"></div></div><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box default-icon" style=""  ><div class="porto-sicon-default"><div id="porto-icon-7977985985ba4a59296831" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
										<div class="porto-icon circle "  style="color:#ffffff;background:#288e56;font-size:14px;display:inline-block;">
											<i class="fa fa-map-marker"></i>
										</div></div></div></div><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="font-size:14px;color:#777777;"><strong>Địa chỉ:</strong> Tố Hữu, Hà Đông, Hà Nội</h3></div> <!-- header --></div> <!-- porto-sicon-box --></div><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box default-icon" style=""  ><div class="porto-sicon-default"><div id="porto-icon-16205562105ba4a592976b6" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
											<div class="porto-icon circle "  style="color:#ffffff;background:#288e56;font-size:14px;display:inline-block;">
												<i class="fa fa-phone"></i>
											</div></div></div></div><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="font-size:14px;color:#777777;"><strong>Điện thoại:</strong>0987 09 9199</h3></div> <!-- header --></div> <!-- porto-sicon-box --></div><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box default-icon" style=""  ><div class="porto-sicon-default"><div id="porto-icon-15891636625ba4a59298391" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
												<div class="porto-icon circle "  style="color:#ffffff;background:#288e56;font-size:14px;display:inline-block;">
													<i class="fa fa-envelope"></i>
												</div></div></div></div><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="font-size:14px;color:#777777;"><strong>Email PKD:</strong> dinhkhanhkn@gmail.com</h3></div> <!-- header --></div> <!-- porto-sicon-box --></div></div></div></div></div></div>
												@endsection