@extends('homeAdmin')

@section('css')
@endsection

@section('container')
<script>
  var options = {
    filebrowserImageBrowseUrl: '/lte/public/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/lte/public/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/lte/public/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/lte/public/laravel-filemanager/upload?type=Files&_token='
  };
</script>
<script src="//cdn.ckeditor.com/4.10.1/full/ckeditor.js"></script>
<div class="card">
	<div class="card-header bg-primary">
		<h4>Edit Project</h4>
	</div>
	<div class="card-body">
		<form action="" method="post" enctype="">
			@csrf
	<div class="form-group row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<label for="name">{{ __('Name Project') }}</label>
			<input type="text" name="name" id="name" class="form-control" value="<?php echo isset($project)?$project->name:'' ?>">
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="form-group row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<label for="introduction">{{ __('Introduction') }}</label>
			<textarea id="introduction" name="introduction" rows="3" class="form-control">
				<?php echo isset($project)?$project->introduction:'' ?>
			</textarea>
			<script >
				CKEDITOR.replace('introduction',options);
			</script>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="form-group row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<label for="description">{{ __('Description') }}</label>
			<textarea id="description" name="description" rows="3" class="form-control">
				<?php echo isset($project)?$project->description:'' ?>
			</textarea>
			<script >
				CKEDITOR.replace('description',options);
			</script>
		</div>
		<div class="col-md-2"></div>
		</div>

	<div class="form-group row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<label for="name">{{ __('Payment Policy') }}</label>
			<textarea class="form-control" id="payment" name="payment" rows="4">
				<?php echo isset($project)?$project->payment:'' ?>
			</textarea>
			<script >
				CKEDITOR.replace('payment',options);
			</script>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="form-group row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<label for="policy">{{ __('Policy') }}</label>
			<textarea name="policy" id="policy" rows='4'>
				<?php echo isset($project)?$project->policy:'' ?>
			</textarea>
			<script>
				CKEDITOR.replace('policy',options);
			</script>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="form-group row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<label for="ground">{{ __('Ground') }}</label>
			<textarea name="ground" id="ground" rows='4'>
				<?php echo isset($project)?$project->ground:'' ?>
			</textarea>
			<script>
				CKEDITOR.replace('ground',options);
			</script>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="form-group row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<label for="location">{{ __('Location') }}</label>
			<textarea name="location" id="location" rows='4'>
				<?php echo isset($project)?$project->location:'' ?>
			</textarea>
			<script>
				CKEDITOR.replace('location',options);
			</script>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="form-group row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<label for="content">{{ __('Content') }}</label>
			<textarea name="content" id="content" rows='4'>
				<?php echo isset($project)?$project->content:'' ?>
			</textarea>
			<script>
				CKEDITOR.replace('content',options);
			</script>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="form-group row">
		<div class="col-md-4"></div>
		<div class="col-md-2"> 
			<button class="btn btn-success" type="submit">Submit</button>
		</div>
		<div class="col-md-2">
			<button class="btn btn-danger" type="reset">Reset</button>
		</div>
		<div class="col-md-4"></div>
	</div>
</form>
	</div>
</div>

@endsection

@section('js')

@endsection