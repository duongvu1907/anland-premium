@extends('homeAdmin')

@section('container')
<div class="card">
	<div class="card-header">
		<h4></h4>
	</div>
	<div class="card-body">
		<form action="" method="post" enctype="multipart/form-data">
			@csrf
			<div class="form-group row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<label>Name</label>
					<input type="text" name="name" placeholder="name property" class="form-control" required="">
				</div>
				<div class="col-md-3"></div>
			</div>
			<div class="form-group row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<select name="imageId" required="">
						<option value="">Choose images</option>
						<?php foreach ($image as $p): ?>
							<option value="{{ $p->id }}">{{ $p->name }}</option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-md-3"></div>
			</div>
			<div class="form-group row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<label>Location</label>
					<input type="text" name="location" class="form-control" required="">
				</div>
				<div class="col-md-3"></div>
			</div>
			<div class="form-group row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<select name="propertyId" required="">
						<option value="">Choose property</option>
						<?php foreach ($property as $p): ?>
							<option value="{{ $p->id }}">{{ $p->name }}</option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-md-3"></div>
			</div>
			
			<div class="form-group row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
				<div class="col-md-3">
					<button class="btn btn-danger" type="reset">Reset</button>
				</div>
				<div class="col-md-3"></div>
			</div>
	</form>
	</div>
</div>

<div class="card">
	<div class="card-header">
		<h4>List porperty</h4>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Image</th>
						<th>Property</th>
						<td>Location</td>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($location as $p): ?>
						<tr>
							<td>{{ $p->id }}</td>
							<td>{{  $p->name }}</td>
							<td>{{ $p->image->name }}</td>
							<td>{{ $p->property->name }}</td>
							<td>{{ $p->location }}</td>
							<td style="font-size: 25px">
								<a href="{{ url('admin/location/delete/'.$p->id) }}"><span class="fa fa-times"></span></a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<div class="pagination">
				{{ $location->render() }}
			</div>
		</div>
	</div>
</div>
@endsection