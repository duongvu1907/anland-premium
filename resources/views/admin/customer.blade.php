@extends('homeAdmin')

@section('container')
	<div class="card">
		<div class="card-header">
			<h4>List customer</h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Region</th>
							<th>IP</th>
							<th>Message</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($customer as $c): ?>
							<tr>
								<td>{{ $c->id }}</td>
								<td>{{ $c->name }}</td>
								<td>{{ $c->email }}</td>
								<td>{{ $c->phone }}</td>
								<td>{{ $c->region }}</td>
								<td>{{ $c->ip }}</td>
								<td><?php echo $c->message==''?'[ No Message ]':$c->message ?></td>
								<td style="font-size: 20px;color: red;">
									<a href="{{ url('admin/customer/delete/'.$c->id) }}"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							<?php DB::table('customer')->where("id","=",$c->id)->update(['status'=>0]) ?>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<div class="pagination">
				{{$customer->render()}}
			</div>
		</div>
	</div>
@endsection