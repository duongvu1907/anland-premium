@extends('homeAdmin')
@section('container')
<form action="" enctype="multipart/form-data" method="post">
@csrf
<div class="card">
	<div class="card-header">
		<h4>Upload</h4>
	</div>
	<div class="card-body">
		<div class="form-group row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<input type="file" name="image" accept="image/*" id="imageFile" required="">
			</div>
			<div class="col-md-2">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-2"></div>
			<div class="col-md-8" id="edit_file">
				<input type="text" name="name" id="fileName" class="form-control" placeholder="***file name">
			</div>
			<div class="col-md-2"></div>
		</div>

		<div class="form-group row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<input type="text" name="description" id="description" class="form-control" required="" placeholder="***description">
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="form-group row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<select required name="location" id="location" >
					<option value="">Choose location</option>
					<option value="bg1">Background 01</option>
					<option value="bg2">Background 02</option>
					<option value="bg3">Background 03</option>
					<option value="bg4">Background 04</option>
					<option value="bg5">Background 05</option>
					<option value="banner">Banner</option>
					<option value="house">House template</option>
				</select>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="form-group row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<button type="submit" class="btn btn-danger" >Upload</button>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
</form>  
<div class="card">
	<div class="card-header" style="background: #00cec9">
		<h4>List house image</h4>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th >#</th>
						<th>Image</th>
						<th>Description</th>
						<th>Updated at</th>
						<th>Location</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($house as $i): ?>
						<tr>
							<td class="id">{{ $i->id }}</td>
							<td><img width="150px" src="{{ asset('public/photos/shares/apartment/'.$i->name) }}" alt=""></td>
							<td>{{ $i->description }}</td>
							<td>{{$i->updated_at }}</td>
							<td>{{$i->location }}</td>
							<td style="font-size: 20px" class="text-center"><span class="fa fa-circle"  onclick="editStatus(this)" @if($i->status==1) style="color: green;cursor:pointer " @else style='color:red;cursor:pointer' @endif></span></td>
							<td>
							<a href="{{ url('admin/real-estate/upload/delete/'.$i->id) }}"><i class=" fa fa-trash delete" style="cursor:pointer;"></i></a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>  
<div class="card">
	<div class="card-header" style="background: #00cec9">
		<h4>List backgorund</h4>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Image</th>
						<th>Description</th>
						<th>Updated at</th>
						<th>Location</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($bg as $i): ?>
						<tr>
							<td class="id">{{ $i->id }}</td>
							<td><img width="150px" src="{{ asset('public/photos/shares/apartment/'.$i->name) }}" alt=""></td>
							<td>{{ $i->description }}</td>
							<td>{{$i->updated_at }}</td>
							<td style="font-weight: bold;color: green;text-transform: uppercase;">{{$i->location }}</td>
							<td style="font-size: 20px" class="text-center"><span onclick="editStatus(this)" class="fa fa-circle status" @if($i->status==1) style="color: green;cursor:pointer " @else style='color:red;cursor:pointer' @endif></span></td>
							<td>
							<a href="{{ url('admin/real-estate/upload/delete/'.$i->id) }}"><i class=" fa fa-trash delete" style="cursor:pointer;"></i></a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>  
@endsection

@section('js')

<script>
	$('#imageFile').change(function(e){
            var fileName = e.target.files[0].name;
            $("#fileName").val(fileName);
        });


	function editStatus(btn){
		var id =	$(btn).parent("td").parent("tr").find('.id').html();
		var Url =  "<?php echo url('admin/real-estate/upload/editStatus/') ?>";
		$.ajax({
			url:Url+"/"+id,
			type: 'get',
			dataType: 'html',
			data: {"id": id},
			success:function(data){
				if(data==1){
					$(btn).css('color', 'green');
				}else 
					$(btn).css('color', 'red');
			}
		});
	}
</script>
@endsection