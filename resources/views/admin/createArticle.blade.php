@extends('homeAdmin')
@section('css')
	<style>
		.card-header{
			text-transform: uppercase;
			font-size: 20px;

		}
	</style>
@endsection
@section('container')
	<script src="{{ asset('public/tinymce/tinymce.min.js')}}"></script>
	<div class="card">
                <div class="card-header">{{ $card }} Post</div>

                <div class="card-body">
                    <form method="POST" action="" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="title" class="col-sm-4 col-form-label text-md-right">{{ __('Title Post') }}</label>
                            <div class="col-md-6">
                                <input id="title" type="text" name="title" value="<?php echo isset($post)?$post->title:'' ?>" required autofocus class="form-control">
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="description" class="col-sm-4 col-form-label text-md-right">{{ __('Description') }}</label>
                            <div class="col-md-6">
                               <textarea name="description" id="description" class="form-control" rows="10">
                               	<?php echo isset($post)?$post->description:'' ?>
                               </textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="title" class="col-sm-4 col-form-label text-md-right">{{ __('Keywords') }}</label>
                            <div class="col-md-6">
                                <textarea name="keywords" id="keywords" class="form-control text-left" rows="5" required="">
                                	<?php 
                                  if(isset($post))
                                  {
                                    $keywords = json_decode($post->keywords);
                                    foreach ($keywords as $key) {
                                       echo $key.",";
                                     } 
                                  }
                                   ?>
                                </textarea>
                            </div>
                        </div>
						
						<div class="form-group row">
							<label for="thumb" class="col-sm-4 col-form-label text-md-right">{{ __('Thumbnail') }}</label>
                            <div class="col-md-6">
                               <input type="file" name="thumb" @if(!isset($post->id)) required @endif accept="image/*">
                            </div>
						</div>

						<div class="form-group row">
							<div class="col-md-1"></div>
                            <div class="col-md-10">
                                <textarea name="content" class="form-control my-editor" rows="30" required="">
                                	<?php echo isset($post)?$post->content:'' ?>
                                </textarea>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
	
						<div class="form-group row">
							<div class="col-md-4"></div>
							<div class="col-md-2">
								<button class="btn btn-success text-uppercase" type="submit">{{ $card }}</button>
							</div>
							<div class="col-md-2">
								<button class="btn btn-danger text-uppercase" type="reset">Reset</button>
							</div>
							<div class="col-md-4"></div>
						</div>

                    </form>
                </div>
            </div>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'lte/public/laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endsection
@section('js')
	
@endsection