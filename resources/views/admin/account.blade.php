@extends('homeAdmin')
@section('container')
	<div class="card">
		<div class="card-header">
			<h4>Account</h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Email</th>
							<th>Premisison</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
							<?php foreach ($user as $u): ?>
								<tr>
									<td>{{ $u->id }}</td>
									<td>{{ $u->email }}</td>
									<td>{{ $u->premission }}</td>
									<td>
										<a href="{{ url('admin/delete/user/'.$u->id) }}"><span class="fa fa-times"></span></a>
									</td>
							</tr>
							<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection