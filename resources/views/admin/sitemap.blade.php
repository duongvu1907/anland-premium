@extends('homeAdmin')

@section('container')
	<div class="card">
		<div class="card-header">
			<h4>Site map</h4>
		</div>
		<div class="card-body">
			<form action=""  enctype="multipart/form-data" method="post">
				@csrf
			<div class="form-group row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<input type="file" name="sitemap">
				</div>
				<div class="col-md-4">
					<button class="btn btn-success" type="submit">Upload</button>
				</div>
			</div>
			</form>
		</div>
	</div>

@endsection