@extends('homeAdmin')

@section('container')
<script>
  var options = {
  	disallowedContent: 'img{width,height}',
    filebrowserImageBrowseUrl: '/lte/public/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/lte/public/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/lte/public/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/lte/public/laravel-filemanager/upload?type=Files&_token='
  };
</script>
<script src="//cdn.ckeditor.com/4.10.1/full/ckeditor.js"></script>
<div class="card">
	<div class="card-header">
		<h4></h4>
	</div>
	<div class="card-body">
		<form action="{{ url('admin/property/') }}" method="post">
			@csrf
			<div class="form-group row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<label>Name</label>
					<input type="text" name="name" placeholder="name property" class="form-control">
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="form-group row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<label>Content</label>
					<textarea class="form-control content" name="content"  rows="10"></textarea>
					<script>
						CKEDITOR.replace('content',options);
					</script>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="form-group row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
				<div class="col-md-3">
					<button class="btn btn-danger" type="reset">Reset</button>
				</div>
				<div class="col-md-3"></div>
			</div>
	</form>
	</div>
</div>
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg ">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      	<form action="{{ url('admin/property/edit/') }}" method="post">
			@csrf
			<input type="text" hidden="" id="id" name="id" >
			<div class="form-group row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<label>Name</label>
					<input type="text" name="name" id="name" placeholder="name property" class="form-control">
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="form-group row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<label>Content</label>
					<textarea class="form-control content" name="content" id="content" rows="10"></textarea>
					<script type="text/javascript">
						CKEDITOR.replace('content',options);
					</script>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="form-group row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
				<div class="col-md-3">
					<button class="btn btn-danger" type="reset">Reset</button>
				</div>
				<div class="col-md-3"></div>
			</div>
	</form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<script>
	function edit(btn)
	{
		var id = $(btn).parent("td").parents("tr").find('.idProperty').text();
		var name = $(btn).parent("td").parents("tr").find('.nameProperty').text()
		var content = $(btn).parent("td").parents("tr").find('.contentProperty').html();
		$("#id").attr('value', id);
		$("#name").val(name);
		$("#myModal").modal('show');
	}
</script>
<div class="card">
	<div class="card-header">
		<h4>List porperty</h4>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>content</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($property as $p): ?>
						<tr>
							<td class="idProperty">{{ $p->id }}</td>
							<td class="nameProperty">{{ $p->name }}</td>
							<td class="contentProperty">{{ $p->content }}</td>
							<td style="font-size: 25px">
								<span class="fa fa-edit edit" onclick="edit(this)"></span> &nbsp;&nbsp;&nbsp;
								<a href="{{ url('admin/property/delete/'.$p->id) }}"><span class="fa fa-times"></span></a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection