@extends('homeAdmin')
@section('css')
<style>
.attributes-user input{
	width: 80%;
	height: 40px;
	padding: 0px 10px;
	background: #F4F7FA;
}
.fa{
	cursor: pointer;
	margin-left: 5px;
}
label{
	font-size: 18px;
}
.info-user
{
	margin-bottom: 30px;
}
#change-password{
	color: #3498db;
	cursor: help;
}
#change-password:hover{
	color: #e74c3c;
}

#password input
{	
	display: none;
	width: 200px;
	background: #fff;
}
#password button
{
	display: none;
	margin-top: 20px;
}
@media (max-width: 580)
{
	#password input{
		display: block;
	}
}
</style>
@endsection
@section('container')
<div class="row">
	<div class="col-md-3">
	</div>
	<div class="col-md-6" style="min-height: 1000px">
		<div class="avatar text-center">
			<img src="{{ asset('public/upload/admin/'.$user->image) }}" class="img-fluid rounded-circle mCS_img_loaded" alt="Avatar" width="150px">
			<br>
		</div>
		<div class=" text-center" id="loader">
		</div>
		<hr>

		<div class="info-user">
			<div class="attributes-user">
				<div class="col-xs-6">
					<label for="name"><i class="fa fa-user"></i>&nbsp;<strong>Name</strong> </label>&nbsp;&nbsp;&nbsp;
					<input type="text" value="{{ $user->name }}" name="name" readonly id="name"><i class=" fa fa-wrench wrench"></i>
				</div>
				<div class="col-xs-6 btn-update text-center">
					
				</div>
			</div>
		</div>

		<div class="info-user">
			<div class="attributes-user">
				<div class="col-xs-6">
					<label><i class="fa fa-envelope"></i>&nbsp;<strong>Email</strong> &nbsp;&nbsp;&nbsp; {{ $user->email }}</label>
					
				</div>
				<div class="col-xs-6 text-center">
				</div>
			</div>
		</div>

		<div class="info-user">
			<div class="attributes-user">
				<div class="col-xs-6">
					<label for="created_at"><i class="fa fa-calendar"></i>&nbsp;<strong>Created at</strong> :&nbsp; {{ $user->created_at}} 
						<br>
						<br>
						<i class="fa fa-calendar"></i> <strong>Updated at</strong> :&nbsp;{{ $user->updated_at }} </label>

					</div>
					<div class="col-xs-6"></div>
				</div>
			</div>

			<div class="info-user">
				<div class="attributes-user">
					<div class="col-xs-6">
						<label for="phone"><i class="fa fa-phone"></i>&nbsp;<strong>Phone</strong> </label>&nbsp;&nbsp;&nbsp;
						<input type="text" value="{{ $user->phone }}" id="phone" name="phone" readonly/><i class=" fa fa-wrench wrench"></i>
					</div>
					<div class="col-xs-6 btn-update text-center"></div>
				</div>
			</div>

			<div class="info-user">
				<div class="attributes-user">
					<div class="col-xs-6">
						<label><i class="fa fa-user"></i>&nbsp;<strong>Premission</strong> &nbsp;&nbsp;&nbsp; <strong>{{ $user->premission }}</strong> </label>
					</div>
					<div class="col-xs-6"></div>
				</div>
			</div>

			<div class="info-user">
				<div class="attributes-user">
					<div class="col-xs-6">
						<label id="change-password"><i class="fa fa-key"></i>&nbsp;<strong onclick="slideDownPass()">Change password</strong></label>
					</div>
					<div class="col-xs-6"></div>
				</div>
			</div>
			<div class="info-user text-center" id="password">
				<hr>
				<div class="attributes-user">
					<label>Password : </label>&nbsp;<input type="password" name="password" id="txt_password">
					<br><br>
					<label for="">Confirm :</label> &nbsp;<input type="password" name="confirm_password" id="txt_confirm_password" id="">
				</div>
				<script>

				</script>
				<button class="btn btn-success" onclick="submit(this)" ><i class="fa fa-file"></i>&nbsp;Update</button>
				<button class="btn btn-danger" onclick="slideUpPass()" type="reset"><i class="fa fa-times"></i>&nbsp;Cancel</button>
				<div class="alert-password"></div>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
	@section('js')
	<script>
		function submit(btn){
			var password = $(btn).parent("#password").find('#txt_password').val();
			var confirm_password = $(btn).parent("#password").find('#txt_confirm_password').val();
			if (password==confirm_password && password.search(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,10}$/g)==0) {
				var data ={
					"id":"password",
					"password":password,
					"_token": '<?php echo csrf_token() ?>'
				};
				ajaxHttp(JSON.stringify(data),"<?php echo url('admin/profile') ?>",'post');
				$(".alert-password").html('');
			}else{
				$(".alert-password").html("<p style='color:red;'><i class='fa fa-times'></i>&nbsp;Please! min:8 character,a special character,a number</p>");
			}
		}
	</script>
	<script src="{{asset('public/js/loader.js')}}"></script>
	<script>
		jQuery(document).ready(function($) {
			$("#password input").css('display', 'inline-table');
			$("#password button").css('display', 'inline-table');
			$("#password").slideUp(0);
		});

		$(".wrench").click(function(event) {
			var input = $(this).parents(".attributes-user").find('input');
			input.css('background', '#fff');
			input.removeAttr('readonly');
			$(this).parents(".attributes-user").children('.btn-update').html("");
			$(this).parents(".attributes-user").children('.btn-update').append("<button class='btn btn-success' onclick='updateSend(this)' style='margin-top:20px' >Update</button>&nbsp;&nbsp;&nbsp;<button class='btn btn-danger' style='margin-top:20px' onclick='cancel(this)'>Cancel</button>");
		});
		function slideDownPass()
		{
			$("#password").slideDown('normal');
		}
		function slideUpPass()
		{
			$("#password").slideUp('normal');
			$("#password input").val("");
			$(".alert-password").html('');
		}
		function updateSend(btn)
		{
			var _token = '<?php echo csrf_token() ?>'
			var input = $(btn).parents('.attributes-user').find('input');
			var url = "<?php echo url('admin/profile') ?>";
			switch (input.attr('id')) {
				case 'name':
				var name = input.val();
					var data = {
						"id":"name",
						"name":name,
						"_token":_token
					}
					ajaxHttp(JSON.stringify(data),url,"post");
					input.css('background', '#ebebeb');
					input.attr('readonly', '');
					$(btn).parents(".btn-update").html("");
				break;
				case "phone":
				var phone = input.val();
				if (phone.search(/(09|01[2|6|8|9])+([0-9]{8})\b/g)==0) 
				{
					var data = {
						'id':"phone",
						"phone":phone,
						"_token":_token
					}
					ajaxHttp(JSON.stringify(data),url,"post");
					input.css('background', '#ebebeb');
					input.attr('readonly', '');
					$(btn).parents(".btn-update").html("");
				}
				
				break;
			}

		// input.css('background', '#ebebeb');
		// input.attr('readonly', '');
		// $(btn).parents(".btn-update").html("");
	}
	function cancel(btn){
		var input = $(btn).parents(".attributes-user").find('input');
		var name = input.attr('id');
		switch (name) {
			case 'name':
			input.val('<?php echo $user->name ?>');
			break;
			case 'phone':
			input.val("<?php echo $user->phone ?>");
			break;
		}
		input.css('background', '#ebebeb');
		input.attr('readonly', '');
		$(btn).parents(".btn-update").html("");
	}

</script>
@endsection
@endsection