@extends('homeAdmin')
@section('css')
@endsection
@section('container')
	<button style="margin-bottom: 50px" class="btn btn-success" onclick="window.location.href='<?php echo url('admin/real-estate/edit') ?>'">Edit</button>
	<br>
	<hr>
	<div class="card">
		<div class="card-header bg-primary">
			<h4>Name</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<?php echo $project->name ?>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header bg-primary">
			<h4>Introduction</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<?php echo $project->introduction ?>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header bg-primary">
			<h4>Description</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<?php echo $project->descrition ?>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header bg-primary">
			<h4>Payment Policy</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<?php echo $project->payment ?>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header bg-primary">
			<h4>Policy</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<?php echo $project->policy ?>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header bg-primary">
			<h4>Ground</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<?php echo $project->ground ?>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header bg-primary">
			<h4>Location</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<?php echo $project->location ?>
				</div>    
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header bg-primary">
			<h4>Content</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<?php echo $project->content ?>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
	
@endsection
@section('js')
@endsection