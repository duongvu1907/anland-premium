@extends('homeAdmin')

@section('container')
<div class="card">
	<div class="card-header">
		<h4></h4>
	</div>
	<div class="card-body">
		<form action="" method="post" enctype="multipart/form-data">
			@csrf
			<div class="form-group row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<label>Choose image</label>
					<input type="file" name="name">
				</div>
				<div class="col-md-3"></div>
			</div>
			<div class="form-group row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
				<div class="col-md-3">
					<button class="btn btn-danger" type="reset">Reset</button>
				</div>
				<div class="col-md-3"></div>
			</div>
	</form>
	</div>
</div>

<div class="card">
	<div class="card-header">
		<h4>List porperty</h4>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($image as $p): ?>
						<tr>
							<td>{{ $p->id }}</td>
							<td>{{ $p->name }}</td>
							<td><img src="{{ asset('public/photos/shares/property/'.$p->name) }}" alt="" width="200px"></td>
							<td style="font-size: 25px">
								<span class="fa fa-edit edit"></span> &nbsp;&nbsp;&nbsp;
								<a href="{{ url('admin/image-property/delete/'.$p->id) }}"><span class="fa fa-times"></span></a>
							</td> 
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection