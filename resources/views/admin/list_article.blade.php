@extends('homeAdmin')

@section('css')

@endsection
@section('container')
<div class="card">
	<div class="card-header bg-success">
		<h4>List Posts</h4>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-hover table-sm">
				<thead>
					<tr>
						<th >#</th>
						<th>Title</th>
						<th>Author</th>
						<th>Keywords</th>
						<th>Update at</th>
						<th class="text-center">Status</th>
						<th>Thumb</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($posts as $post): ?>
						
					<tr >
						<td scope="row" class="id">{{ $post->id }}</td>
						<td>{{ $post->title }}</td>
						<td>{{ $users->find($post->userId)->name }}</td>
						<td>
							<?php $keywords = json_decode($post->keywords);
								foreach ($keywords as $keyword) {
									echo $keyword.",";
								}

							 ?>
						</td>
						<td>{{ $post->updated_at }}</td>
						<td style="font-size: 20px" class="text-center"><span class="fa fa-circle" onclick="editStatus(this)" @if($post->status==1) style="color: green;cursor:pointer " @else style='color:red;cursor:pointer' @endif></span></td>
						<td><img src="{{ asset('public/thumbs/'.$post->thumb) }}" width="100px" alt="{{ $createAlt->alt($post->title) }}"></td>
						<td class="text-uppercase;" style="font-size: 20px;color:red">
							<a href="{{ url('admin/article/edit/'.$post->id) }}" target="__blank"><i class="fa fa-edit edit" style="cursor: pointer;"></i></a>&nbsp;&nbsp;
							<a href="{{ url('admin/article/delete/'.$post->id) }}"><i class=" fa fa-trash delete" style="cursor:pointer;"></i></a>
						</td>
					</tr>
					
					<?php endforeach ?>
				</tbody>
			</table>
			</div>
		<div class="row text-right" style="padding: 15px">
			
				{{ $posts->render() }}
		
		</div>
	</div>
</div>
@endsection
@section('js')
<script>
	function editStatus(btn){
		var id =	$(btn).parent("td").parent("tr").find('.id').html();
		var Url =  "<?php echo url('admin/ajax/editStatus/') ?>";
		$.ajax({
			url:Url+"/"+id,
			type: 'get',
			dataType: 'html',
			data: {"id": id},
			success:function(data){
				if(data==1){
					$(btn).css('color', 'green');
				}else 
					$(btn).css('color', 'red');
			}
		})
	}
</script>
@endsection