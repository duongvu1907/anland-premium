@extends('homeAdmin')

@section('css')

@endsection
@section('container')
<div class="card">
	<div class="card-header bg-success">
		<h4>List metas</h4>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-hover table-sm" id="table-meta">
				<thead>
					<tr>
						<th>#</th>
						<th>Location</th>
						<th>Author</th>
						<th>Content</th>
						<th>Update at</th>
						<th class="text-center">Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="data-meta">
					<?php foreach ($metas as $meta): ?>
						
					<tr class="<?php echo 'meta-'.$meta->id ?>">
						<td scope="row" class="id">{{ $meta->id }}</td>
						<td class="location">{{ $meta->location }}</td>
						<td>{{ $user->find($meta->userId)->name }}</td>
						<td class="content">{{ $meta->content }}</td>
						<td>{{ $meta->updated_at }}</td>
						<td style="font-size: 20px" class="text-center">
								<i class="fa fa-circle" onclick="editStatus(this)" @if($meta->status==1) style="color: green;cursor:pointer " @else style='color:red;cursor:pointer' @endif></i></td>
						<td class="text-uppercase;" style="font-size: 20px;color:red">
							<i class="fa fa-edit edit" style="cursor: pointer;"></i>&nbsp;&nbsp;
							<a href="{{ url('admin/meta/delete/'.$meta->id) }}"><i class=" fa fa-trash delete" style="cursor:pointer;"></i></a>
						</td>
					</tr>
					
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
		<div class="row text-right" style="padding: 15px">
			
				{{ $metas->render() }}
		
		</div>
	</div>
</div>


<div class="card"  id="editStatus">
	<div class="card-header bg-success">
		<h4>Create metas</h4>
	</div>
	<div class="card-body">
		<button class="btn btn-success" onclick="Create()">Clear & Create new Meta</button>
			<div class="form-group row" >
				<div class="col-md-2 loader"></div>
				<div class="col-md-8">
					<label for="location">Location</label>
					<select id="location" name="location">
						<option value="home">Home</option>
						<option value="post">Post</option>
					</select>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="form-group row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="content">Content:</label>
					<div class="input-group">
						<input type="text" name="content" id="content" class="form-control">
						<div class="input-group-append">
							<button id="submit" type="button" class="btn btn-success">Submit</button>
						</div>
					</div>
					<div class="decode" style="visibility: hidden;display: none;"></div>
				</div>
				<div class="col-md-2">
					<input type="number" hidden id="id" name="id">
				</div>
			</div>
	</div>
</div>
@endsection
@section('js')
<script>
	function Create()
	{
		$("#content").val("");
		$("#id").val("");
	}
	function editStatus(btn){
		var id =	$(btn).parent("td").parent("tr").find('.id').html();
		var Url =  "<?php echo url('admin/meta/editStatus/') ?>";
		$.ajax({
			url:Url+"/"+id,
			type: 'get',
			dataType: 'html',
			data: {"id": id},
			success:function(data){
				if(data==1){
					$(btn).css('color', 'green');
				}else 
					$(btn).css('color', 'red');
			}
		})
	}
	$(".edit").click(function(event) {
		var id = $(this).parent("td").parent("tr").find('.id').html();
		var content = $(this).parent("td").parent("tr").find('.content').html();
		var location  =$(this).parent("td").parent("tr").find('.location').html();
		$("#location").val(location);
		$("#id").val(id);
		$(".decode").html(content);
		$("#content").val($(".decode").text());
	});
	$("#submit").click(function(event) {
		var location  = $("#location").val();
		var id  = $("#id").val();
		var content  = $("#content").val();
		var url = "<?php echo url('admin/meta/edit') ?>";
		if(content!=""&&id!=""){
			var data = {
				"id":id,
				"action":"edit",
			"location":location,
			"content":content,
			"_token":"<?php echo csrf_token() ?>"
			}
			xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function(){
				if(this.readyState==4 && this.status ===200)
				{ 
					var meta  = JSON.parse(this.responseText);
					var cl = ".meta-"+meta.id;
					$(cl).children('.location').text(meta.location);
					$(cl).children('.content').text(meta.content);
					
					$(".loader").html("");
				}else{
					$('.loader').html("<img src='https://upload.wikimedia.org/wikipedia/commons/c/c7/Loading_2.gif' width='50px' />")
				}
			}
			xmlhttp.open("post",url,true);
			xmlhttp.setRequestHeader('Content-Type', 'application/json');
			xmlhttp.send(JSON.stringify(data));
		}else{
			
			var data = {
				"location":location,
				"content":content,
				"action":"add",
				"_token":"<?php echo csrf_token() ?>"
			}
			xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function(){
				if(this.readyState==4 && this.status ===200)
				{ 
					var meta = JSON.parse(this.responseText);
					var content = unescape(meta.content);
					var cl = "meta-"+meta.id;
					$("#data-meta").append("<tr class='"+cl+"'><td scope='row' class='id'>"+meta.id+"</td><td class='location'>"+meta.location+"</td><td>Duong Vu</td><td class='content'></td><td>"+meta.updated_at+"</td><td style='font-size: 20px' class='text-center'><i class='fa fa-circle' onclick='editStatus(this)' style='color: green;cursor:pointer '></i></td><td class='text-uppercase;' style='font-size: 20px;color:red'><i class='fa fa-edit edit' style='cursor: pointer;'></i>&nbsp;&nbsp;<a href='<?php url('admin/meta/delete/'.$meta->id) ?>'><i class=' fa fa-trash delete' style='cursor:pointer;'></i></a></td></tr>")
					$(".meta-"+meta.id).children('.content').text(meta.content);
					$(".loader").html("");
				}else{
					$('.loader').html("<img src='https://upload.wikimedia.org/wikipedia/commons/c/c7/Loading_2.gif' width='50px' />")
				}
			}
			xmlhttp.open("post",url,true);
			xmlhttp.setRequestHeader('Content-Type', 'application/json');
			xmlhttp.send(JSON.stringify(data));

		}

	});
</script>
@endsection