<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns#">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <!--[if IE]>
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
@yield('meta')
<link rel="shortcut icon" href="{{ asset('public/photos/shares/banner/favicon.png?timestamp=1537787348')}}" type="image/x-icon" />
<link rel="apple-touch-icon" href="#">
<link rel="apple-touch-icon" sizes="120x120" href="#">
<link rel="apple-touch-icon" sizes="76x76" href="#">
<link rel="apple-touch-icon" sizes="152x152" href="#">

<title>Dự án Anland Premium - Nam Cuong</title>
<style rel="stylesheet" property="stylesheet" type="text/css">
    .ms-loading-container .ms-loading, .ms-slide .ms-slide-loading{ background-image: none !important; background-color: transparent !important; box-shadow: none !important; }
     #header .logo { max-width: 164px; } 
     @media (min-width: 1200px) { #header .logo { max-width: 164px; } } 
     @media (max-width: 991px) { #header .logo { max-width: 110px; } } 
     @media (max-width: 767px) { #header .logo { max-width: 110px; } } 
    img.wp-smiley,
    img.emoji {
     display: inline !important;
     border: none !important;
     box-shadow: none !important;
     height: 1em !important;
     width: 1em !important;
     margin: 0 .07em !important;
     vertical-align: -0.1em !important;
     background: none !important;
     padding: 0 !important;
    }
</style>

    <link rel='stylesheet'  href="{{ asset('public/homePage/plugins/popup-builder/css/theme459e.css?ver=3.0.4')}}" type='text/css' media='all' />
    <link rel='stylesheet'   href="{{ asset('public/homePage/plugins/popup-builder/css/animate459e.css?ver=3.0.4')}}" type='text/css' media='all' />
    <link rel='stylesheet'  href="{{ asset('public/homePage/plugins/js_composer/css/js_composer.min.css?ver=5.4.5')}}" type='text/css' media='all' />

    <link rel='stylesheet' id='porto-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A200%2C300%2C400%2C700%2C800%2C600%7CShadows+Into+Light%3A200%2C300%2C400%2C700%2C800%2C600%7C&amp;subset=cyrillic%2Ccyrillic-ext%2Cgreek%2Cgreek-ext%2Ckhmer%2Clatin%2Clatin-ext%2Cvietnamese&amp;ver=4.9.8' type='text/css' media='all' />
    
    <link rel='stylesheet'   href="{{ asset('public/homePage/themes/css/bootstrap_15010.css?ver=4.9.8')}}" type='text/css' media='all' />
    <link rel='stylesheet'   href="{{ asset('public/homePage/themes/css/plugins5010.css?ver=4.9.8')}}" type='text/css' media='all' />
    <link rel='stylesheet'   href="{{ asset('public/homePage/themes/css/theme5010.css?ver=4.9.8')}}" type='text/css' media='all' />
    <link rel='stylesheet'   href="{{ asset('public/homePage/themes/css/dynamic_style_15010.css?ver=4.9.8')}}" type='text/css' media='all' />
    <link rel='stylesheet'  href="{{ asset('public/homePage/themes/css/skin_15010.css?ver=4.9.8')}}" type='text/css' media='all' />
    <link rel='stylesheet'  href="{{ asset('public/homePage/themes/style5010.css?ver=4.9.8')}}" type='text/css' media='all' />

    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">

    <link rel='stylesheet' property='stylesheet'  href="{{ asset('public/homePage/plugins/js_composer/css/js_composer_tta.min.css?ver=5.4.5')}}" type='text/css' media='all' />
    <link rel='stylesheet' property='stylesheet'  href="{{ asset('public/homePage/plugins/js_composer/lib/vc_carousel/css/vc_carousel.min.css?ver=5.4.5')}}" type='text/css' media='all' />

<script type='text/javascript' src="{{asset('public/core/vendor/jquery/jquery.min.js')}}">
</script>
<script type='text/javascript' src="{{asset('public/core/vendor/jquery-migrate/jquery-migrate.min.js')}}">
</script>
<!-- <script type='text/javascript' src="{{ asset('public/homePage/plugins/popup-builder/js/Popup.js?ver=3.0.4')}}">
</script>
<script type='text/javascript' src="{{ asset('public/homePage/plugins/popup-builder/js/PopupConfig.js?ver=3.0.4')}}">
</script>
<script type='text/javascript' src="{{ asset('public/homePage/plugins/popup-builder/js/PopupBuilder.js?ver=3.0.4')}}">
</script> -->
<script type='text/javascript' src="{{ asset('public/homePage/themes/js/popper.min.js?ver=4.2')}}">
</script>
<script type='text/javascript' src="{{ asset('public/homePage/themes/js/bootstrap.min.js?ver=4.2')}}">
</script>
<script type='text/javascript' src="{{ asset('public/homePage/themes/js/plugins.min.js?ver=4.2')}}">
</script>
<link rel='https://api.w.org/' href='wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="" /> 
<meta name="generator" content="WordPress 4.9.8" />
<link rel='shortlink' href='' />
<meta name="generator" content=""/>
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="{{ asset('public/homePage/plugins/js_composer/assets/css/vc_lte_ie9.min.css')}}" media="screen">
<![endif]-->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
@yield('css')
</head>
<body class="home page-template-default page page-id-598 full blog-1  wpb-js-composer js-comp-ver-5.4.5 vc_responsive">

    <div class="page-wrapper">
        <!-- page wrapper -->


        <div class="header-wrapper clearfix">
            <!-- header wrapper -->

            <header id="header" class="header-separate header-corporate header-17 search-sm sticky-menu-header">

                <div class="header-main">
                    <div class="container">
                        <div class="header-left">
                            <h1 class="logo">
                                <a href="{{ url('/') }}" title="" rel="home">
                                <img class="img-responsive standard-logo" src="http://localhost/lte/public/photos/shares/photos/anland.png?timestamp=1537106085" alt="" />
                                <img class="img-responsive retina-logo" src="http://localhost/lte/public/photos/shares/photos/anland.png?timestamp=1537106085" alt="" style="display:none;" /> 
                            </a>
                        </h1>            </div>
                        <div class="header-right">
                            <div class="header-contact">
                                <div class="feature-box">
                                    <div class="feature-box-icon">
                                        <?php $banner = DB::table('image_house')->where('location','=','banner')->first() ?>
                                        <a href="">
                                            <img src="{{ asset('public/photos/shares/apartment/'.$banner->name) }}" width="728" height="60" alt="{{ $banner->description }}">
                                        </a>
                                    </div>
                                </div>
                            </div>                                <a class="mobile-toggle">
                                <i class="fa fa-reorder">
                                </i>
                            </a>

                        </div>
                    </div>

                    <div id="nav-panel" class="">
                        <div class="container">
                            <div class="mobile-nav-wrap">
                                <div class="menu-wrap">
                                    <ul id="menu-main-menu" class="mobile-menu accordion-menu">
                                        <li id="accordion-menu-item-754" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-598 current_page_item ">
                                            <a href="{{ url('') }}" rel="nofollow" class=" current ">Trang chủ</a>
                                        </li>
                                        <li id="accordion-menu-item-755" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home ">
                                            <a href="{{ url('') }}#vi-tri" rel="nofollow" class=" current ">Vị trí</a>
                                        </li>
                                        <li id="accordion-menu-item-756" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home ">
                                            <a href="{{ url('') }}#mat-bang" rel="nofollow" class=" current ">Mặt bằng</a>
                                        </li>
                                        <li id="accordion-menu-item-757" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home ">
                                            <a href="{{ url('') }}#thiet-ke" rel="nofollow" class=" current ">Thiết kế</a>
                                        </li>
                                        <li id="accordion-menu-item-758" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home ">
                                            <a href="{{ url('') }}#tien-ich" rel="nofollow" class=" current ">Tiện ích</a>
                                        </li>
                                        <li id="accordion-menu-item-759" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home ">
                                            <a href="{{ url('') }}#tien-do" rel="nofollow" class=" current ">Tiến độ thanh toán</a>
                                        </li>
                                        <li id="accordion-menu-item-145" class="menu-item menu-item-type-post_type menu-item-object-page ">
                                            <a href="{{ url('/tin-tuc') }}" rel="nofollow" class="">Tin tức dự án</a>
                                        </li>
                                        <li id="accordion-menu-item-144" class="menu-item menu-item-type-post_type menu-item-object-page ">
                                            <a href="{{ url('/lien-he') }}" rel="nofollow" class="">Liên hệ</a>
                                        </li>
                                    </ul>
                                </div> 
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="main-menu-wrap menu-flat menu-flat-border">
                        <div id="main-menu" class="container ">
                            <div class="menu-center">
                                <ul id="menu-main-menu-1" class="main-menu mega-menu menu-flat menu-flat-border show-arrow effect-down subeffect-fadein-left">
                                    <li id="nav-menu-item-754" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-598 current_page_item  narrow ">
                                        <a href="{{ url('') }}" class=" current">Trang chủ</a>
                                    </li>
                                    <li id="nav-menu-item-755" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home  narrow ">
                                        <a href="{{ url('') }}#vi-tri" class=" current">Vị trí</a>
                                    </li>
                                    <li id="nav-menu-item-756" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home  narrow ">
                                        <a href="{{ url('') }}#mat-bang" class=" current">Mặt bằng</a>
                                    </li>
                                    <li id="nav-menu-item-757" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home  narrow ">
                                        <a href="{{ url('') }}#thiet-ke" class=" current">Thiết kế</a>
                                    </li>
                                    <li id="nav-menu-item-758" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home  narrow ">
                                        <a href="{{ url('') }}#tien-ich" class=" current">Tiện ích</a>
                                    </li>
                                    <li id="nav-menu-item-759" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home  narrow ">
                                        <a href="{{ url('') }}#tien-do" class=" current">Tiến độ thanh toán</a>
                                    </li>
                                    <li id="nav-menu-item-145" class="menu-item menu-item-type-post_type menu-item-object-page  narrow ">
                                        <a href="{{ url('/tin-tuc') }}" class="">Tin tức dự án</a>
                                    </li>
                                    <li id="nav-menu-item-144" class="menu-item menu-item-type-post_type menu-item-object-page  narrow ">
                                        <a href="{{ url('/lien-he') }}" class="">Liên hệ</a>
                                    </li>
                                </ul>                </div>
                                <div class="menu-right">
                                </div>
                            </div>
                        </div>

                    </header>
                </div>
                <!-- end header wrapper -->

            <div class="page-content">
                @yield('main')
            </div>

                <div class="footer-wrapper ">



                    <div id="footer" class="footer-3">
                        <div class="footer-main">
                            <div class="container">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <aside id="block-widget-2" class="widget widget-block">            <div class="block">
                                            <div class="porto-block ">
                                                <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row row pre-footer-bg m-t-none vc_custom_1536404187592 m-t-none m-b-none p-t-none p-b-none section-no-borders vc_row-has-fill">
                                                    <div class="vc_column_container col-md-4">
                                                        <div class="wpb_wrapper vc_column-inner">
                                                            <div class="vc_row wpb_row vc_inner row">
                                                                <div class="vc_column_container col-md-12">
                                                                    <div class="wpb_wrapper vc_column-inner">
                                                                        <h4  class="vc_custom_heading m-b-xs align-left">Địa chỉ liên hệ</h4>
                                                                        <div class="wpb_text_column wpb_content_element " >
                                                                          <div class="wpb_wrapper">
                                                                           <p>Dự án Anland Premium</p>
                                                                           <p>
                                                                            <i class="fa fa-home">
                                                                            </i> Ðịa chỉ: Đường Tố Hữu, Dương Nội, Hà Đông.</p>

                                                                        </div>
                                                                    </div>
                                                                    <h4  class="vc_custom_heading m-b-xs align-left">Hotline</h4>
                                                                    <div class="wpb_text_column wpb_content_element  m-b-md" >
                                                                      <div class="wpb_wrapper">
                                                                       <p>
                                                                        <a class="text-decoration-none" title="Hotline" href="tel:0987099199" rel="noopener">
                                                                            <span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">
                                                                                <i class="fa fa-mobile"> </i>Điện thoại:
                                                                                <span class="info text-lg"> 0987 099 199</span>
                                                                            </span>
                                                                        </a>
                                                                    </p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_column_container col-md-4">
                                            <div class="wpb_wrapper vc_column-inner">
                                                <div class="vc_row wpb_row vc_inner row">
                                                    <div class="vc_column_container col-md-12">
                                                        <div class="wpb_wrapper vc_column-inner">
                                                            <h4  class="vc_custom_heading m-b-xs align-left">Cam kết bán hàng</h4>
                                                            <div class="wpb_text_column wpb_content_element " >
                                                              <div class="wpb_wrapper">
                                                               <p>
                                                                <strong>&#x2666; </strong>Cung cấp những thông tin nhanh chóng &amp; cập nhật nhất từ chủ đầu tư<br />
                                                                <strong>&#x2666; </strong>Hỗ trợ Quý khách lựa chọn căn hộ, biệt thự vị trí đẹp nhất<br />
                                                                <strong>&#x2666; </strong>Hỗ trợ tư vấn trực tiếp chuyên sâu<br />
                                                                <strong>&#x2666; </strong>Xem căn hộ mẫu trực tiếp<br />
                                                                <strong>&#x2666; </strong>Không thu thêm bất cứ khoản phí nào. Hỗ trợ làm thủ tục trực tiếp với chủ đầu tư, trước và sau bán hàng lâu dài<br />
                                                                <strong>&#x2666; </strong>Đồng hành cùng khách hàng cho đến khi nhận nhà</p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_column_container col-md-4">
                                        <div class="wpb_wrapper vc_column-inner">
                                            <h4 style="text-align: center" class="vc_custom_heading m-b-xs">Nhận báo giá</h4>
                                            <div role="form" class="wpcf7" id="wpcf7-f128-o5" lang="en-US" dir="ltr">
                                                <div class="screen-reader-response">
                                                </div>
                                                <form action="{{ url('/do-contact') }}" method="post" class="wpcf7-form" novalidate="novalidate">
                                                    <div class="form-group">
                                                        <div class="custom-input-box">
                                                            <i class="Simple-Line-Icons-user">
                                                            </i>
                                                            <span class="wpcf7-form-control-wrap your-name">
                                                                <input type="text" name="name" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="Họ tên*" required="" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-input-box">
                                                            <i class="Simple-Line-Icons-phone">
                                                            </i>
                                                            <span class="wpcf7-form-control-wrap tel-261">
                                                                <input type="number" name="phone" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" required="" placeholder="Số điện thoại*" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-input-box">
                                                            <i class="Simple-Line-Icons-envelope">
                                                            </i>
                                                            <span class="wpcf7-form-control-wrap your-email">
                                                                <input type="email" name="email" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="Email*" required="" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="submit-btn">
                                                        <center>
                                                            <input type="submit" value="ĐĂNG KÝ NGAY" class="wpcf7-form-control wpcf7-submit" />
                                                        </center>
                                                    </div>
                                                    
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vc_row-full-width vc_clearfix">
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>

        </div>
    </div>

</div>

</div>


</div>
<!-- end wrapper -->


<!--[if lt IE 9]>
<script src="{{ asset('public/homePage/themes/js/html5shiv.min.js')}}">
</script>
<script src="{{ asset('public/homePage/themes/js/respond.min.js')}}">
</script>
<![endif]-->

<!-- Modal -->
<div id="contact-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="sgpb-main-html-content-wrapper">
                <p style="text-align: center;">
                    <span style="font-size: 22px;">BẢNG GIÁ - CHÍNH SÁCH</span>
                </p>
                <div>
                    <span >&#x1f31f; Đăng ký nhận ưu đãi từ chủ đầu tư</span>
                </div>
                <div>
                    <span >&#x1f31f; Chính sách bán hàng mới nhất</span>
                </div>
                <div>
                    <span >&#x1f31f; Bảng giá tòa Phú Thanh và Phú Thượng</span>
                </div>
                <div>
                    <span >&#x1f31f; HĐMB + Phụ lục nội thất Full</span>
                </div>
                <div>
                    <span >&#x1f31f; Mặt bằng và chi tiết căn hộ</span>
                </div>
                <p>&nbsp;</p>
                <div>
                    <div role="form" class="wpcf7" id="wpcf7-f128-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response">
                        </div>
                        <form action="{{ url('/do-contact') }}" method="post" class="wpcf7-form" novalidate="novalidate">
                            @csrf
                            <div class="form-group">
                                <div class="custom-input-box">
                                    <i class="Simple-Line-Icons-user">
                                    </i>
                                    <span class="wpcf7-form-control-wrap your-name">
                                        <input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Họ tên*" required="" />
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-input-box">
                                    <i class="Simple-Line-Icons-phone">
                                    </i>
                                    <span class="wpcf7-form-control-wrap tel-261">
                                        <input type="number" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel form-control" aria-required="true" aria-invalid="false" placeholder="Số điện thoại*"  required="" />
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-input-box">
                                    <i class="Simple-Line-Icons-envelope">
                                    </i>
                                    <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" placeholder="Email*"  required="" />
                                    </span>
                                </div>
                            </div>
                            <div class="submit-btn">
                                <center>
                                    <input type="submit" value="ĐĂNG KÝ NGAY" class="wpcf7-form-control wpcf7-submit" />
                                </center>
                            </div>
                            <div class="wpcf7-response-output wpcf7-display-none">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
    setTimeout(function(){
        $("#contact-modal").modal('show');
    },4000);
</script>
<script type='text/javascript' src="{{ asset('public/homePage/plugins/js_composer/js/dist/js_composer_front.min5243.js?ver=5.4.5')}}">
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var js_porto_vars = {"rtl":"","change_logo":"0","container_width":"1170","grid_gutter_width":"30","show_sticky_header":"1","show_sticky_header_tablet":"1","show_sticky_header_mobile":"1","category_ajax":"1","prdctfltr_ajax":"","show_minicart":"0","slider_loop":"0","slider_autoplay":"0","slider_autoheight":"1","slider_speed":"5000","slider_nav":"1","slider_nav_hover":"0","slider_margin":"0","slider_dots":"0","slider_animatein":"","slider_animateout":"","product_thumbs_count":"4","product_zoom":"1","product_zoom_mobile":"1","product_image_popup":"1","zoom_type":"inner","zoom_scroll":"1","zoom_lens_size":"200","zoom_lens_shape":"square","zoom_contain_lens":"1","zoom_lens_border":"1","zoom_border_color":"#888888","zoom_border":"0","screen_lg":"1200","mfp_counter":"%curr% of %total%","mfp_img_error":"<a href=\"%url%\">The image<\/a> could not be loaded.","mfp_ajax_error":"<a href=\"%url%\">The content<\/a> could not be loaded.","popup_close":"Close","popup_prev":"Previous","popup_next":"Next","request_error":"The requested content cannot be loaded.<br\/>Please try again later."};
    /* ]]> */
</script>
<script type='text/javascript' src="{{ asset('public/homePage/themes/js/theme.min.js?ver=4.2')}}">
</script>
<script type='text/javascript' src="{{ asset('public/homePage/plugins/js_composer/lib/bower/zoom/jquery.zoom.min5243.js?ver=5.4.5')}}">
</script>
<script type='text/javascript' src="{{ asset('public/homePage/plugins/js_composer/lib/vc_image_zoom/vc_image_zoom.min5243.js?ver=5.4.5')}}">
</script>
<script type='text/javascript' src="{{ asset('public/homePage/plugins/js_composer/lib/vc_accordion/vc-accordion.min5243.js?ver=5.4.5')}}">
</script>
<script type='text/javascript' src="{{ asset('public/homePage/plugins/js_composer/lib/vc-tta-autoplay/vc-tta-autoplay.min5243.js?ver=5.4.5')}}">
</script>
<script type='text/javascript' src="{{ asset('public/homePage/plugins/js_composer/lib/vc_tabs/vc-tabs.min5243.js?ver=5.4.5')}}">
</script>
<script type='text/javascript' src="{{ asset('public/homePage/plugins/js_composer/lib/vc_carousel/js/transition.min5243.js?ver=5.4.5')}}">
</script>
<script type='text/javascript' src="{{ asset('public/homePage/plugins/js_composer/lib/vc_carousel/js/vc_carousel.min5243.js?ver=5.4.5')}}">
</script>
@yield('js')
<div class="site-support">
 <div class="site-content">
     <a href="#">
        <div class="item toogle-support-item">
         <i class="fa fa-map-marker" aria-hidden="true">
         </i>
         <span>Chung cư Anland Premium</span>
     </div>
 </a>
 <a href="tel:0987099199">
    <div class="item toogle-support-item">
     <i class="fa fa-phone" aria-hidden="true">
     </i>
     <span>Hotline: <strong>0987 099 199</strong>
     </span>
 </div>
</a>

<a href="#dang-ky-ngay" class="porto-popup-content" data-animation="my-mfp-zoom-in">
    <div class="item chat toogle-support-item">
        <div class="download-fill">
        </div>
        <i class="fa fa-download" aria-hidden="true">
        </i>
        <span>
            <blink>TẢI MẶT BẰNG - BẢNG GIÁ - CHÍNH SÁCH</blink>
        </span>
    </div>
</a>
<div id="dang-ky-ngay" class="dialog zoom-anim-dialog mfp-hide">
 <div class="wpb_text_column wpb_content_element " >
  <div class="wpb_wrapper">
   <h2 style="text-align: center;">
    <span style="color: #ffffff;">Quý khách vui lòng điền chính xác thông tin. Hệ thống sẽ phản hồi lại cho quý khách trong 5 phút</span>
</h2>
<div role="form" class="wpcf7" id="wpcf7-f128-o6" lang="en-US" dir="ltr">
    <div class="screen-reader-response">
    </div>
    <form action="{{ url('/do-contact/') }}" method="post" >
        @csrf
        <div class="form-group">
            <div class="custom-input-box">
                <i class="Simple-Line-Icons-user">
                </i>
                <span class="wpcf7-form-control-wrap your-name">
                    <input type="text" name="name" value="" size="40" class="w form-control" aria-required="true" aria-invalid="false" placeholder="Họ tên*" required="" />
                </span>
            </div>
        </div>
        <div class="form-group">
            <div class="custom-input-box">
                <i class="Simple-Line-Icons-phone">
                </i>
                <span class="wpcf7-form-control-wrap tel-261">
                    <input type="number" name="phone" value="" size="40" class=" form-control" aria-required="true" aria-invalid="false" required="" placeholder="Số điện thoại*" />
                </span>
            </div>
        </div>
        <div class="form-group">
            <div class="custom-input-box">
                <i class="Simple-Line-Icons-envelope">
                </i>
                <span class="wpcf7-form-control-wrap your-email">
                    <input type="email" name="email" value="" size="40" class=" form-control" aria-required="true" aria-invalid="false" placeholder="Email*" required="" />
                </span>
            </div>
        </div>
        <div class="submit-btn">
            <center>
                <input type="submit" value="ĐĂNG KÝ NGAY" class="wpcf7-form-control wpcf7-submit" />
            </center>
        </div>
    </form>
</div>		</div>
</div>
</div>

<div class="contact-fill">
</div>
<a href="tel:0987099199">
    <div class="item hidden-pc">
     <i class="fa fa-phone" aria-hidden="true">
     </i>
 </div>
</a>
</div>
</div>
<div class="lvchatnew">
 <div class="lvc_mb">
  <a href="#dang-ky-ngay" class="porto-popup-content dbtn kn2bt red btnda2tv" data-animation="my-mfp-zoom-in">
   <img src="{{ asset('public/homePage/themes/images/regist-icon.png') }}">TẢI TÀI LIỆU
</a>
<a href="tel:0987099199" class="dbtn kn1bt" >
   <img src="{{ asset('public/homePage/themes/images/call.png') }}">0987 099 199
</a>
</div>
</div>
</body>

</html>