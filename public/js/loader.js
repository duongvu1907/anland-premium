function ajaxHttp(Data,Url,Method)
{
	xmlhttp = new  XMLHttpRequest();
	xmlhttp.onreadystatechange = function(){
		if (this.readyState==4 && this.status==200) {
			removeLoader();
		}else {
			addLoader();
		}
	};
	xmlhttp.open(Method,Url,true);
	xmlhttp.setRequestHeader('Content-Type', 'application/json')
	xmlhttp.send(Data);	
}
function addLoader()
{
	var src = window.location.hostname+'/public/loader.gif';
	document.getElementById('loader').innerHTML="<img src='http://superstorefinder.net/support/wp-content/uploads/2018/01/blue_loading.gif' width='40px'/>"
}
function removeLoader()
{
	$("#loader").html("");
}