<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $table = 'metas';

    protected $fillable = [
    	"location","content","userId","status"
    ];
    public $timestamps = true;
    public function user()
    {
    	return $this->belongsTo('App\User','userId');
    }
}
