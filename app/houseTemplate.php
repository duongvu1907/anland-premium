<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class houseTemplate extends Model
{
    protected $table = "image_house";
    protected $fillable = [
    	'name','description','status','location'
    ];
    public $timestamps =true;
}
