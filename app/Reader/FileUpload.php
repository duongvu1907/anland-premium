<?php 
 namespace App\Reader;
 use File;
 class FileUpload
 {
 	public static function upload($file,$path)
 	{
 		try {
 			$name = $file->getClientOriginalName();
 			if(File::exists($path."/".$name))
 			{
 				$name = str_random(5)."_".$name;
 			}
 			$file->move($path,$name);
 			return $name;
 		} catch (Exception $e) {
 			echo $e->getMessage();
 		}
 	}
 	public static function delete($name,$path)
 	{
 		try {
 			if(File::exists($path.'/'.$name))
 			{
 				File::delete($path.'/'.$name);
 			}
 		} catch (Exception $e) {
 			echo $e->getMessage();
 		}
 	}
 }