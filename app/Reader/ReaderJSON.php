<?php
 namespace App\Reader;
 use File;
 	class ReaderJSON
 	{
 		public static  function Geographic_Vietnam()
 		{
 			if(File::exists('public/json/Geographic_Vietnam.json'))
 			{
 				try {
 					return json_decode(file_get_contents(asset('public/json/Geographic_Vietnam.json')));
 				} catch (Exception $e) {
 					return $e->getMessage();
 				}
 			}else
 				return abort(404,'File not found');
 		}
 	} 