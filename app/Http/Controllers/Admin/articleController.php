<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reader\FileUpload;
use App\Reader\CreateAlt;
use App\Post;
use App\User;
use Auth;
class articleController extends Controller
{
	function __construct()
	{
        $this->createAlt = new CreateAlt;
	}
	public function list_article(Request $request)
	{
		$data["posts"] = Post::paginate(15);
		$data["users"] = new User;
		$data['createAlt'] = new CreateAlt;
		return view("admin.list_article",$data);
	}
    public function view_create()
    {
    	$data["card"] = "create";
    	return view('admin.createArticle',$data);
    }
    public function create(Request $request)
    {
    	$title = $request->title;
    	$keywords = explode(",",$request->keywords);
    	$description = $request->description;
    	$content = $request->content;
    	$thumb = $request->thumb;
    	$post  = new Post;

    	$post->title = $title;
    	$post->keywords = json_encode($keywords);
    	$post->description = $description;
    	$post->content = $content;
    	$post->userId = Auth::user()->id;
    	$post->status = 1;
    	$post->thumb = FileUpload::upload($thumb, "public/thumbs");
        $post->url = $this->createAlt->alt($title);
    	$post->save();
    	return redirect(url('admin/article'));
    }
    public function edit_article($id)
    {
    	$data['post'] = Post::find($id);
    	$data['card'] = "Edit";
    	return view('admin.createArticle',$data);
    }
    public function do_edit(Request $request,$id)
    {
    	$post = Post::find($id);
    	if($request->hasFile('thumb'))
    	{
    		$file = $request->thumb;
    		FileUpload::delete($post->thumb, "public/thumbs");
    		

    		$post->title = $request->title;
    		$post->description = $request->description;
    		$post->content = $request->content;
    		$post->thumb = FileUpload::upload($file, "public/thumbs");
    		$post->keywords = json_encode(explode(",", $request->keywords));
    		$post->userId = Auth::user()->id;
            $post->url = $this->createAlt->alt($request->title);
    		$post->save();
    		return redirect(url('admin/article'));
    	}else{
    		$post->title = $request->title;
    		$post->description = $request->description;
    		$post->content = $request->content;
    		$post->keywords = json_encode(explode(",", $request->keywords));
    		$post->userId = Auth::user()->id;
            $post->url = $this->createAlt->alt($request->title);
    		$post->save();
			return redirect(url('admin/article'));

    	}
    	
    }
    public function do_status($id)
    {
    	$post = Post::find($id);
    	if($post->status==1)
    	{
    		$post->status =0;$post->save();
    		return $post->status;
    	}else{
    		$post->status=1;$post->save();
    		return $post->status;
    	}
    }
    public function delete($id)
    {
        FileUpload::delete(Post::find($id)->thumb, 'public/thumbs');
    	Post::find($id)->delete();
    	return redirect(url('admin/article'));
    }
}
