<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
class customerController extends Controller
{
    public function customer()
    {
    	$data['customer'] = Customer::orderBy('id','desc')->paginate(15);
    	
    	return view('admin.customer',$data);
    }
    public function delete($id)
    {
    	Customer::find($id)->delete();
    	return redirect(url('admin/customer'));
    }
}
