<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\houseTemplate;
use App\Reader\FileUpload;
use App\Reader\CreateAlt;
use File;
use DB;
class projectController extends Controller
{
    public function edit()
    {
    	$data['project'] = Project::first();
       
    	return view('admin.view_project',$data);
    }
    public function do_edit(Request $request)
    {
        $project =  Project::find(1);
    	$project->name =  $request->name;
    	$project->introduction =  $request->introduction;
    	$project->description =  $request->description;
    	$project->payment =  $request->payment;
    	$project->policy =  $request->policy;
    	$project->content = $request->content;
    	$project->ground = $request->ground;
        $project->location = $request->location;
    	$project->save();
    	return redirect(url('admin/real-estate'));
    }
    public function project()
    {
    	$data['project']= Project::first();

    	return view('admin.real-estate',$data);
    }
    public function upload()
    {
        $data['house'] = houseTemplate::where('location','house')->get();
        $data['bg'] = DB::table('image_house')->where('location','like','bg%')->get();
        return view("admin.upload",$data);
    }
    public function do_upload(Request $request)
    {
        // if(isset(houseTemplate::where('location','')))
        $file = $request->image;
        $name = isset($request->name)?$request->name:'';
        if($name==''){
            $name = $file->getClientOriginalName();
        }
        if(File::exists('public/photos/shares/apartment/'.$name))
        {
            $name = str_random(5)."_".$name;
        }

        $file->move('public/photos/shares/apartment/',$name);

        $location = $request->location;
        if($location!='house' && houseTemplate::where('location',$location)->first()!=null){
            $house = houseTemplate::where('location',$location)->first();
            File::delete('public/photos/shares/apartment/'.$house->name);
            $house->name = $name;
            $house->description = $request->description;
            $house->save();
        }else{
            $house = new houseTemplate;
            $house->name = $name;
            $house->location = $location;
            $house->description = $request->description;
            $house->status = 1;
            $house->save();
        }
        return redirect(url('admin/real-estate/upload'));
    }
    public function editStatus($id){
        $i = houseTemplate::find($id);
        if($i->status==1){
            $i->status=0;
            $i->save();
        }else{
            $i->status =1;
            $i->save();
        }
        return $i->status;
    }
    public function delete($id)
    {
        FileUpload::delete(houseTemplate::find($id)->name, "public/photos/shares/apartment");
        houseTemplate::find($id)->delete();
        return redirect(url('admin/real-estate/upload'));
    }
}

