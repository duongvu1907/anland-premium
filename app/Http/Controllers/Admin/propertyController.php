<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Property;
use App\ImageProperty;
use App\LocationPro;
use App\Reader\FileUpload;
class propertyController extends Controller
{
    public function add()
    {
    	$data['property'] = Property::paginate(15);
    	return view('admin.add_property',$data);
    }
    public function do_add(Request $request)
    {
    	$property = new Property;
    	$property->name = $request->name;
    	$property->content = $request->content;
    	$property->save();
    	return redirect(url('admin/property/'));
    } 
    public function location()
    {
    	$data['image'] = ImageProperty::get();
    	$data['property'] = Property::get();
    	$data['location'] = LocationPro::paginate(15);
    	return view('admin.add_location',$data);
    }
    public function do_location(Request $request)
    {
    	$location = new LocationPro($request->all());
    	$location->save();
    	$data['image'] = ImageProperty::get();
    	$data['property'] = Property::get();
    	$data['location'] = LocationPro::paginate(15);
    	return view('admin.add_location',$data);
    }
    public function image()
    {
    	$data['image'] = ImageProperty::get();
    	return view('admin.add_image',$data);
    }
    public function do_image(Request $request)
    {
    	$image = new ImageProperty;
    	$image->name = FileUpload::upload($request->name,'public/photos/shares/property/');
    	$image->save();
    	return redirect(url('admin/image-property'));
    }
    public function delete_image($id)
    {
         $l = LocationPro::get();
        foreach ($l as $v) {
            if($v->image->id==$id)
                $v->delete();
        }
    	FileUpload::delete(ImageProperty::find($id)->name,'public/photos/shares/property/');
    	ImageProperty::find($id)->delete();

    	return redirect(url('admin/image-property'));
    }
    public function delete_property($id)
    {
    	Property::find($id)->delete();
    	return redirect(url('admin/property'));
    }
    public function delete_location($id)
    {
    	LocationPro::find($id)->delete();
    	return redirect(url('admin/location'));
    }

    public function edit_property(Request $request)
    {
    	$property = Property::find($request->id);
    	$property->name = $request->name;
    	$property->content = $request->content;
    	$property->save();
    	return redirect(url('admin/property'));
    }
}
