<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\User;
use Auth;
use Hash;
use File;
class homeController extends Controller
{
	function __construct()
	{

	}
    public function home()
    {
    	return view('admin.statistics');
    }
    public function profile()
    {
    	$data["user"] = Auth::user();
    	return view('admin.profile',$data);
    }
    public function update(Request $request)
    {

    	switch ($request->id) {
    		case 'name':
    			Auth::user()->name = $request->name;
    			Auth::user()->save();
    			break;
    		case 'phone':
    			Auth::user()->phone = $request->phone;
    			Auth::user()->save();
    			break;
    		case 'password':
    			Auth::user()->password = Hash::make($request->password);
    			Auth::user()->save();
    	}
    }
    public function dashboard()
    {
        return view('admin.dashboard');
    }
    public function sitemap()
    {
        return view('admin.sitemap');
    }
    public function upload_sm(Request $request)
    {
        $file = $request->sitemap;
        if(File::exists('public/../sitemap.xml')){
            File::delete('public/../sitemap.xml');
        }
        $file->move('public/../','sitemap.xml');
        return redirect(url('/sitemap.xml'));
    }
    public function delete_account()
    {
        if(Auth::user()->checkPremission('admin')){
        $data['user'] = User::where('id','<>',Auth::user()->id)->get();
        return view('admin.account',$data);
        }else{
            return abort(404);
        }
    }
    public function delete($id)
    {
        if(Auth::user()->checkPremission('admin')){
            User::find($id)->delete();
            return redirect(url('admin/delete-user'));
        }else{
            return abort(404);
        }
    }
}

