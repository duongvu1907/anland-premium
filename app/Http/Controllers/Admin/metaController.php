<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Meta;
use App\User;
use Auth;
class metaController extends Controller
{
    public function list_meta()
    {
    	$data['metas'] = Meta::paginate(10);
    	$data['user'] = new User;
    	return view('admin.list_meta',$data);
    }
    public function do_status($id)
    {
    	$meta = Meta::find($id);
    	if($meta->status==1)
    	{
    		$meta->status =0;$meta->save();
    		return $meta->status;
    	}else{
    		$meta->status=1;$meta->save();
    		return $meta->status;
    	}
    }
    public function edit(Request $request)
    {
    	$action  = $request->action;
    	switch ($action) {
    		case 'edit':
    			$meta = Meta::find($request->id);
		    	$meta->content = $request->content;
		    	$meta->location = $request->location;
		    	$meta->save();
		    	return json_encode($meta);
    			break;
    		case 'add':
    			$meta = new Meta;
    			$meta->content = $request->content;
    			$meta->location = $request->location;
    			$meta->status =1;
    			$meta->userId = Auth::user()->id;
    			$meta->save();
    			return json_encode($meta);

    			break;
    	}
    }
    public function delete($id)
    {
    	Meta::find($id)->delete();
    	return redirect(url('admin/meta'));
    }
}
