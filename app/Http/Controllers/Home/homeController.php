<?php

namespace App\Http\Controllers\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\houseTemplate;
use App\Post;
use App\Reader\CreateAlt;
use DB;
use App\LocationPro;
use App\ImageProperty;
use App\Property;
use App\Customer;
use Location;
use Mail;
use App\Meta;
class homeController extends Controller
{
    function __construct()
    {
        $this->create = new CreateAlt;
    }
    public function index()
    {
    	$data['project'] = Project::first();
    	$data['bg1'] = houseTemplate::where("location","bg1")->where('status','=',1)->first();
        $data['bg2'] = houseTemplate::where("location","bg2")->where('status','=',1)->first();
        $data['bg3'] = houseTemplate::where("location","bg3")->where('status','=',1)->first();
        $data['bg4'] = houseTemplate::where("location","bg4")->where('status','=',1)->first();
        $data['temp'] = houseTemplate::where("location","house")->where('status','=',1)->get();
        $data['post'] = Post::where('status','=',1)->orderBy('id','desc')->take(3)->get();
        $data['createAlt'] = new CreateAlt;
        $data['location'] = LocationPro::get();
        $data['imageProperty'] = ImageProperty::get();
        $data['banner'] = houseTemplate::where("location","banner")->orderBy('id','desc')->first();
        $data['house1'] = houseTemplate::where("location","house")->orderBy('id','desc')->first();
        $data['houseN'] = houseTemplate::where("location","house")->where('id','<',$data['house1']->id)->take(3)->get();


        $data['meta'] = Meta::where('location','home')->where('status','=',1)->get();
    	return view('home.index',$data);
    }
    public function recordNext($id)
    {
    	$post = Post::where("status",'=',1)->where('id',"<",$id)->orderBy('id','desc')->take(3)->get();
    	if($post!=null){
    	return json_decode($post);
    	}else{
    		return null;
    	}
    }
    public function recordPrev($id)
    {
    	$post = Post::where("status",'=',1)->where('id',">",$id)->orderBy('id','desc')->take(3)->get();
    	if($post!=null){
    	return json_decode($post);
    	}else{
    		return null;
    	}
    }
    public function send_property(Request $request)
    {
        return json_encode(Property::find(LocationPro::find($request->id)->property));
    }
    public function contact(Request $request)
    {
        $customer = new Customer;
        $customer->name = $request->name;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->ip = $request->ip();
        $location  = Location::get($request->ip());
        $customer->region= $location->cityName." , ".$location->countryCode.". Vĩ độ ".$location->latitude." | "."Kinh độ ".$location->longitude;
        if(isset($request->message)){
         $customer->message = $request->message;
        }else{
             $customer->message = "Subcribe";
        }
        $customer->status = 1;
        $customer->save();
       

        // $data= [
        //     "title"=>"Notify",
        //     "to_message"=>html_entity_decode($request->message),
        //     "to_email"=>"duongvu.survive@gmail.com",
        //     "to_name"=>$request->name,
        //     "to_phone"=>$request->phone
        // ];
        // // echo html_entity_decode($data["message"]);
        // Mail::send("mails.mail",$data,function($mgn){
        //     $mgn->to("duongvu.survive@gmail.com","Anland Premium")->subject("Anland");
        // });
        

        return redirect(url('/'));
    }
    public function posts(){
        $data['createAlt'] = new CreateAlt;
        $data['posts'] = Post::where('status','=',1)->paginate(15);
        return view('home.posts',$data);        
    }
    public function detail_post($slug)
    {
        $arr = explode("-",$slug);
        $id = $arr[count($arr)-1];
        $url = str_replace("-".$id,'',$slug);
        $post = Post::find($id);

        if($post!=null && strcmp($post->url,$url)==0)
        {
            $data['post'] = $post;
            $data['meta'] = Meta::where('location','post')->where('status','=',1)->get();
            return view('home.detail_post',$data);
        }else{
            return abort(404);
        }
    }
    public function lh()
    {
        return view('home.lienhe');
    }
    
}
