<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";
    protected $fillable=[
    	"title","description","userId","content","status","thumb","keywords",'url'
    ];
    public $timestamps = true;
    public function user()
    {
    	return $this->belongsTo("App\User",'userId');
    }
}
