<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';
    protected $fillable = [
    	"name","introduction","description","payment","policy","ground","content","location"
    ];
    public $timestamps = true;
}
