<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageProperty extends Model
{
    protected $table = 'imageproperty';
    protected $fillable = [
    	"name"
    ];
    public function location()
    {
    	return $this->hasMany('App\LocationPro');
    }
}
