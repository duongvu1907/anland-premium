<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'property';
    protected $fillable = [
    	"name","content"
    ];
    function location()
    {
    	return $this->hasMany('App\LocationPro');
    } 
}
