<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationPro extends Model
{
    protected $table = 'locationpro';
    protected $fillable = [
    	"name",'propertyId','imageId','location'
    ];
    public function image()
    {
    	return $this->belongsTo('App\ImageProperty','imageId');
    }
    public function property()
    {
    	return $this->belongsTo('App\Property','propertyId');
    }
}
